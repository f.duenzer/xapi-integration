var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstvwx",
  1: "acfgijmnprsx",
  2: "x",
  3: "acdefghijmnprsx",
  4: "abcdefgijlmnprstwx",
  5: "cdnps",
  6: "cms",
  7: "cgjlpsv",
  8: "abcdefgilmnprstv",
  9: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

