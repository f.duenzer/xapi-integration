﻿using System;
using System.Threading.Tasks;
using xAPI_Definitions_Parser.Code;
using xAPI_Definitions_Parser.Code.CSharp;
using xAPI_Definitions_Parser.Code.JavaScript;
using xAPI_Definitions_Parser.Code.Python;
using xAPI_Definitions_Parser.Deserialization;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser {
    /// <summary>
    /// Class that provides easy to use access to code generation functionality
    /// </summary>
    public static class Generator {
        /// <summary>
        /// Creates a CodeGenerator
        /// </summary>
        /// <param name="lang">Language</param>
        /// <param name="ns">Namespace</param>
        /// <param name="nsCore">Core namespace</param>
        /// <param name="nsDef">Definitions namespace</param>
        /// <returns>Created CodeGenerator</returns>
        public static CodeGenerator CreateGenerator(CodeLanguage lang, string ns = default, string nsCore = default, string nsDef = default) {
            if (lang == CodeLanguage.CSharp) {
                return new CSharpGenerator(ns, nsCore, nsDef);
            } else if (lang == CodeLanguage.JavaScript) {
                return new JavaScriptGenerator();
            } else if (lang == CodeLanguage.Python) {
                return new PythonGenerator();
            }
            return null;
        }

        /// <summary>
        /// Creates an IOManager
        /// </summary>
        /// <param name="type">Source type</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <returns>Created IOManager</returns>
        public static IOManager CreateManager(SourceType? type, string projectId = default, string privateKey = default, string branch = default) {
            if (type == SourceType.Local) {
                return new FileManager();
            } else if (type == SourceType.GitLab) {
                return string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                            : new GitLabManager(projectId, privateKey, branch);
            }
            return null;
        }

        /// <summary>
        /// Creates CompilationArguments
        /// </summary>
        /// <param name="lang">Language</param>
        /// <param name="dest">Destination</param>
        /// <param name="dotnet">.NET Core version for C#</param>
        /// <param name="refPath">Reference path for C#</param>
        /// <returns>Created CompilationArguments</returns>
        public static CompilationArguments CreateCompilationArguments(CodeLanguage lang, string dest,
            string dotnet = default, string refPath = default) {
            if (lang == CodeLanguage.CSharp) {
                return new CSharpCompilationArguments(dest, dotnet, refPath);
            } else if (lang == CodeLanguage.JavaScript) {
                return new JavaScriptCompilationArguments(dest);
            } else if (lang == CodeLanguage.Python) {
                return new PythonCompilationArguments(dest);
            }
            return null;
        }

        /// <summary>
        /// Generic parse method
        /// </summary>
        /// <param name="source">Source type</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="dest">Destination type</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static void Parse(SourceType source, string path, string projectId, string branch, string privateKey,
            SourceType dest, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var reader = CreateManager(source, projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = source == dest ? reader : CreateManager(dest, projectId, privateKey, branch);
            if (reader != writer && onLog != null) writer.OnLog += onLog;
            ParseInternal(reader, writer, path, lang, ns, output);
        }

        /// <summary>
        /// Generic asynchronous parse method
        /// </summary>
        /// <param name="source">Source type</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="dest">Destination type</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static async Task ParseAsync(SourceType source, string path, string projectId, string branch, string privateKey,
            SourceType dest, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var reader = CreateManager(source, projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = source == dest ? reader : CreateManager(dest, projectId, privateKey, branch);
            if ( reader != writer && onLog != null) writer.OnLog += onLog;
            await ParseInternalAsync(reader, writer, path, lang, ns, output);
        }

        /// <summary>
        /// Parses local files to local output
        /// </summary>
        /// <param name="path">Source path to parse from</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static void ParseLocalToLocal(string path, CodeLanguage lang, string ns,
            string output, Action<string> onLog = null) {
            var manager = new FileManager();
            if (onLog != null) manager.OnLog += onLog;
            ParseInternal(manager, manager, path, lang, ns, output);
        }

        /// <summary>
        /// Parses local files to GitLab output
        /// </summary>
        /// <param name="path">Source path to parse from</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static void ParseLocalToGitLab(string path, string projectId, string branch,
            string privateKey, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var reader = new FileManager();
            if (onLog != null) reader.OnLog += onLog;
            var writer = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) writer.OnLog += onLog;
            ParseInternal(reader, writer, path, lang, ns, output);
        }

        /// <summary>
        /// Parses GitLab files to local output
        /// </summary>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static void ParseGitLabToLocal(string projectId, string branch, string privateKey,
            string path, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var reader = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = new FileManager();
            if (onLog != null) writer.OnLog += onLog;
            ParseInternal(reader, writer, path, lang, ns, output);
        }

        /// <summary>
        /// Asynchronously parses GitLab files to local output
        /// </summary>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static async Task ParseGitLabToLocalAsync(string projectId, string branch, string privateKey,
            string path, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var reader = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = new FileManager();
            if (onLog != null) writer.OnLog += onLog;
            await ParseInternalAsync(reader, writer, path, lang, ns, output);
        }

        /// <summary>
        /// Parses GitLab files to GitLab output
        /// </summary>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static void ParseGitLabToGitLab(string projectId, string branch, string privateKey,
            string path, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var manager = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                               : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) manager.OnLog += onLog;
            ParseInternal(manager, manager, path, lang, ns, output);
        }

        /// <summary>
        /// Asynchronously parses GitLab files to GitLab output
        /// </summary>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="lang">Language to parse to</param>
        /// <param name="ns">Namespace</param>
        /// <param name="output">Location to output parsed content to</param>
        /// <param name="onLog">Logging action</param>
        public static async Task ParseGitLabToGitLabAsync(string projectId, string branch, string privateKey,
            string path, CodeLanguage lang, string ns, string output, Action<string> onLog = null) {
            var manager = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                               : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) manager.OnLog += onLog;
            await ParseInternalAsync(manager, manager, path, lang, ns, output);
        }

        private static void ParseInternal(IOManager reader, IOManager writer, string path,
            CodeLanguage lang, string ns, string output) {
            var contextsRaw = reader.ReadContexts(path);
            var contexts = Deserializer.DeserializeContexts(contextsRaw);
            var generator = CreateGenerator(lang, ns);
            var files = generator.GenerateContexts(contexts);
            var coreFiles = generator.GenerateCoreFiles();
            writer.WriteCodeFiles(output, coreFiles, generator.CoreDirName, files, generator.DefinitionsDirName);
        }

        private static async Task ParseInternalAsync(IOManager reader, IOManager writer, string path,
            CodeLanguage lang, string ns, string output) {
            var contextsRaw = await reader.ReadContextsAsync(path);
            var contexts = Deserializer.DeserializeContexts(contextsRaw);
            var generator = CreateGenerator(lang, ns);
            var files = generator.GenerateContexts(contexts);
            var coreFiles = generator.GenerateCoreFiles();
            await writer.WriteCodeFilesAsync(output, coreFiles, generator.CoreDirName, files, generator.DefinitionsDirName);
        }

        /// <summary>
        /// Compile method
        /// </summary>
        /// <param name="source">Source type</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="dest">Destination type</param>
        /// <param name="ns">Namespace</param>
        /// <param name="args">Compilation arguments</param>
        /// <param name="onLog">Logging action</param>
        public static void Compile(SourceType source, string path, string projectId, string branch, string privateKey,
            SourceType dest, string ns, CompilationArguments args, Action<string> onLog = null) {
            var reader = CreateManager(source, projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            CompileInternal(reader, path, ns, args);
        }

        /// <summary>
        /// Asynchronous compile method
        /// </summary>
        /// <param name="source">Source type</param>
        /// <param name="path">Source path to parse from</param>
        /// <param name="projectId">Project ID for GitLab</param>
        /// <param name="branch">Branch for GitLab</param>
        /// <param name="privateKey">Private key for GitLab</param>
        /// <param name="dest">Destination type</param>
        /// <param name="ns">Namespace</param>
        /// <param name="args">Compilation arguments</param>
        /// <param name="onLog">Logging action</param>
        public static async Task CompileAsync(SourceType source, string path, string projectId, string branch, string privateKey,
            SourceType dest, string ns, CompilationArguments args, Action<string> onLog = null) {
            var reader = CreateManager(source, projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            await CompileInternalAsync(reader, path, ns, args);
        }

        /*public static void CompileLocalToLocal(string path, string ns,
            CompilationArguments args, Action<string> onLog = null) {
            var manager = new FileManager();
            if (onLog != null) manager.OnLog += onLog;
            CompileInternal(manager, manager, path, ns, args);
        }

        public static void CompileLocalToGitLab(string path, string projectId, string branch,
            string privateKey, string ns, CompilationArguments args, Action<string> onLog = null) {
            var reader = new FileManager();
            if (onLog != null) reader.OnLog += onLog;
            var writer = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) writer.OnLog += onLog;
            CompileInternal(reader, writer, path, ns, args);
        }

        public static void CompileGitLabToLocal(string projectId, string branch, string privateKey,
            string path, string ns, CompilationArguments args, Action<string> onLog = null) {
            var reader = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = new FileManager();
            if (onLog != null) writer.OnLog += onLog;
            CompileInternal(reader, writer, path, ns, args);
        }

        public static async Task CompileGitLabToLocalAsync(string projectId, string branch, string privateKey,
            string path, string ns, CompilationArguments args, Action<string> onLog = null) {
            var reader = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                              : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) reader.OnLog += onLog;
            var writer = new FileManager();
            if (onLog != null) writer.OnLog += onLog;
            await CompileInternalAsync(reader, writer, path, ns, args);
        }

        public static void CompileGitLabToGitLab(string projectId, string branch, string privateKey,
            string path, string ns, CompilationArguments args, Action<string> onLog = null) {
            var manager = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                               : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) manager.OnLog += onLog;
            CompileInternal(manager, manager, path, ns, args);
        }

        public static async Task CompileGitLabToGitLabAsync(string projectId, string branch, string privateKey,
            string path, string ns, CompilationArguments args, Action<string> onLog = null) {
            var manager = string.IsNullOrEmpty(branch) ? new GitLabManager(projectId, privateKey)
                                               : new GitLabManager(projectId, privateKey, branch);
            if (onLog != null) manager.OnLog += onLog;
            await CompileInternalAsync(manager, manager, path, ns, args);
        }*/

        private static void CompileInternal(IOManager reader, string path,
            string ns, CompilationArguments args) {
            var contextsRaw = reader.ReadContexts(path);
            var contexts = Deserializer.DeserializeContexts(contextsRaw);
            var generator = CreateGenerator(args.Language, ns);
            var files = generator.GenerateContexts(contexts);
            var coreFiles = generator.GenerateCoreFiles();
            var compiled = generator.Compile(coreFiles, files, args);
            if (compiled.Success && compiled is CSharpCompilationResult res) {
                System.IO.File.WriteAllBytes(args.Destination, res.Compiled);
                System.IO.File.WriteAllText(res.StatementsFile.Path, res.StatementsFile.FileContent);
            }
        }

        private static async Task CompileInternalAsync(IOManager reader, string path,
            string ns, CompilationArguments args) {
            var contextsRaw = await reader.ReadContextsAsync(path);
            var contexts = Deserializer.DeserializeContexts(contextsRaw);
            var generator = CreateGenerator(args.Language, ns);
            var files = generator.GenerateContexts(contexts);
            var coreFiles = generator.GenerateCoreFiles();
            var compiled = generator.Compile(coreFiles, files, args);
            if (compiled.Success && compiled is CSharpCompilationResult res) {
                System.IO.File.WriteAllBytes(args.Destination, res.Compiled);
                System.IO.File.WriteAllText(res.StatementsFile.Path, res.StatementsFile.FileContent);
            }
        }
    }
}