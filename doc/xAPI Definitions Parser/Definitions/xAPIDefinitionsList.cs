﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Definitions {
    /// <summary>
    /// List of xAPIDefinition objects
    /// </summary>
    /// <typeparam name="T">Type of definition</typeparam>
    public class xAPIDefinitionsList<T> where T : xAPIDefinition {
        /// <summary>
        /// Name of the list
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Sub lists of definitions
        /// </summary>
        public IDictionary<string, xAPIDefinitionsList<T>> SubDefinitions { get; } = new Dictionary<string, xAPIDefinitionsList<T>>();

        /// <summary>
        /// List of definitions
        /// </summary>
        public List<T> Definitions { get; } = new List<T>();

        /// <summary>
        /// Adds a definition
        /// </summary>
        /// <param name="definition">Definition</param>
        /// <returns>True, if it was successfully added, false, if not</returns>
        public bool AddDefinition(T definition) {
            bool added = false;
            if (definition != null && (added = !Definitions.Contains(definition))) {
                Definitions.Add(definition);
            }
            return added;
        }

        /// <summary>
        /// Adds a list of sub definitions
        /// </summary>
        /// <param name="name">Name of the list</param>
        /// <param name="definitions">Definitions</param>
        /// <returns>Number of added definitions</returns>
        public bool AddDefinitions(string name, xAPIDefinitionsList<T> definitions) {
            bool added = false;
            if (definitions != null) {
                if (added = !SubDefinitions.ContainsKey(name)) {
                    SubDefinitions.Add(name, definitions);
                }
            }
            return added;
        }
    }
}