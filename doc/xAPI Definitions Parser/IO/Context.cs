﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.IO {
    /// <summary>
    /// Class that represents a context of xAPI definitions
    /// </summary>
    public class Context {
        /// <summary>
        /// Name of the context
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Activity files in the context
        /// </summary>
        public List<FileMeta> Activities { get; } = new List<FileMeta>();
        /// <summary>
        /// Extension files in the context
        /// </summary>
        public IDictionary<string, List<FileMeta>> Extensions { get; } = new Dictionary<string, List<FileMeta>>();
        /// <summary>
        /// Verb files in the context
        /// </summary>
        public List<FileMeta> Verbs { get; } = new List<FileMeta>();

        /// <summary>
        /// Creates a Context
        /// </summary>
        /// <param name="name">Name of the context</param>
        public Context(string name) {
            Name = name;
        }

        /// <summary>
        /// Adds an activity to the context
        /// </summary>
        /// <param name="activity">Activity</param>
        /// <returns>True, if it was successfully added, false, if not</returns>
        public bool AddActivity(FileMeta activity) {
            bool added = false;
            if (activity != null && (added = !Activities.Contains(activity))) {
                Activities.Add(activity);
            }
            return added;
        }

        /// <summary>
        /// Adds activities to the context
        /// </summary>
        /// <param name="activities">A list of activities</param>
        /// <returns>Number of successfully added activities</returns>
        public int AddActivities(List<FileMeta> activities) {
            int added = 0;
            if (activities != null) {
                foreach (var activity in activities) {
                    if (AddActivity(activity)) {
                        ++added;
                    }
                }
            }
            return added;
        }

        /// <summary>
        /// Adds a verb to the context
        /// </summary>
        /// <param name="verb">Verb</param>
        /// <returns>True, if it was successfully added, false, if not</returns>
        public bool AddVerb(FileMeta verb) {
            bool added = false;
            if (verb != null && (added = !Verbs.Contains(verb))) {
                Verbs.Add(verb);
            }
            return added;
        }

        /// <summary>
        /// Adds verbs to the context
        /// </summary>
        /// <param name="verbs">List of verbs</param>
        /// <returns>Number of successfully added verbs</returns>
        public int AddVerbs(List<FileMeta> verbs) {
            int added = 0;
            if (verbs != null) {
                foreach (var verb in verbs) {
                    if (AddVerb(verb)) {
                        ++added;
                    }
                }
            }
            return added;
        }

        /// <summary>
        /// Adds an extension to the context
        /// </summary>
        /// <param name="extension">Extension</param>
        /// <returns>True, if it was successfully added, false, if not</returns>
        public bool AddExtension(string type, FileMeta extension) {
            bool added = false;
            if (!Extensions.ContainsKey(type)) {
                Extensions.Add(type, new List<FileMeta>());
            }
            var extensionsOfType = Extensions[type];
            if (extension != null && (added = !extensionsOfType.Contains(extension))) {
                Extensions[type].Add(extension);
            }
            return added;
        }

        /// <summary>
        /// Adds extensions to the context
        /// </summary>
        /// <param name="extensions">List of extension file objects</param>
        /// <returns>Number of successfully added extensions</returns>
        public int AddExtensions(IDictionary<string, List<FileMeta>> extensions) {
            int added = 0;
            if (extensions != null) {
                foreach (var extType in extensions) {
                    var type = extType.Key;
                    if (!Extensions.ContainsKey(type)) {
                        Extensions.Add(type, new List<FileMeta>());
                    }
                    foreach (var extension in extensions[type]) {
                        if (!Extensions[type].Contains(extension)) {
                            Extensions[type].Add(extension);
                            ++added;
                        }
                    }
                }
            }
            return added;
        }
    }
}