﻿namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// An argument in the syntax tree
    /// </summary>
    public class Argument {
        private string name;
        private object value;

        /// <summary>
        /// Name of the argument
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Value of the argument
        /// </summary>
        public ref readonly object Value => ref value;

        public Argument() { }

        public Argument(object value, string name = default) {
            this.value = value;
            this.name = name;
        }

        public Argument WithName(string name) {
            this.name = name;
            return this;
        }

        public Argument WithValue(object value) {
            this.value = value;
            return this;
        }
    }
}