﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A method in the syntax tree
    /// </summary>
    public class Method {
        private string name;
        private string type;
        private MethodBody body;
        private List<Argument> baseArguments = new List<Argument>();
        private List<Modifier> modifiers = new List<Modifier>();
        private List<Parameter> parameters = new List<Parameter>();

        /// <summary>
        /// Name of the method
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Tyoe of the method
        /// </summary>
        public string Type => type;
        /// <summary>
        /// Body of the method
        /// </summary>
        public ref readonly MethodBody Body => ref body;
        /// <summary>
        /// Arguments for the method's base method
        /// </summary>
        public IReadOnlyList<Argument> BaseArguments => baseArguments.AsReadOnly();
        /// <summary>
        /// Access modifiers of the method
        /// </summary>
        public IReadOnlyList<Modifier> Modifiers => modifiers.AsReadOnly();
        /// <summary>
        /// Parameters of the method
        /// </summary>
        public IReadOnlyList<Parameter> Parameters => parameters.AsReadOnly();

        public Method() { }

        public Method(string name, string type = default, MethodBody body = default) {
            this.name = name;
            this.type = type;
            this.body = body;
        }

        public Method WithName(string name) {
            this.name = name;
            return this;
        }

        public Method WithType(string type) {
            this.type = type;
            return this;
        }

        public Method WithBody(MethodBody body) {
            this.body = body;
            return this;
        }

        public Method AddBaseArgument(Argument arg) {
            baseArguments.Add(arg);
            return this;
        }

        public Method AddBaseArguments(params Argument[] args) {
            baseArguments.AddRange(args);
            return this;
        }

        public Method AddBaseArguments(IEnumerable<Argument> args) {
            baseArguments.AddRange(args);
            return this;
        }

        public Method AddModifier(Modifier mod) {
            modifiers.Add(mod);
            return this;
        }

        public Method AddModifiers(params Modifier[] mods) {
            modifiers.AddRange(mods);
            return this;
        }

        public Method AddModifiers(IEnumerable<Modifier> mods) {
            modifiers.AddRange(mods);
            return this;
        }

        public Method AddParameter(Parameter param) {
            parameters.Add(param);
            return this;
        }

        public Method AddParameters(params Parameter[] parameters) {
            this.parameters.AddRange(parameters);
            return this;
        }

        public Method AddParameters(IEnumerable<Parameter> parameters) {
            this.parameters.AddRange(parameters);
            return this;
        }
    }
}