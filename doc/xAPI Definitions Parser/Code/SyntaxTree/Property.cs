﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A property in the syntax tree
    /// </summary>
    public class Property {
        private string name;
        private string type;
        private List<Modifier> modifiers = new List<Modifier>();
        private List<Argument> arguments = new List<Argument>();
        private string comment;

        /// <summary>
        /// Name of the property
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Type of the property
        /// </summary>
        public string Type => type;
        /// <summary>
        /// Access modifiers of the property
        /// </summary>
        public IReadOnlyList<Modifier> Modifiers => modifiers.AsReadOnly();
        /// <summary>
        /// Arguments to initiate the property with
        /// </summary>
        public IReadOnlyList<Argument> Arguments => arguments.AsReadOnly();
        /// <summary>
        /// Comment of the property
        /// </summary>
        public string Comment => comment;

        public Property() { }

        public Property(string name, string type, string comment = default) {
            this.name = name;
            this.type = type;
            this.comment = comment;
        }

        public Property WithName(string name) {
            this.name = name;
            return this;
        }

        public Property WithType(string type) {
            this.type = type;
            return this;
        }

        public Property AddModifier(Modifier mod) {
            modifiers.Add(mod);
            return this;
        }

        public Property AddModifiers(params Modifier[] mods) {
            modifiers.AddRange(mods);
            return this;
        }

        public Property AddModifiers(IEnumerable<Modifier> mods) {
            modifiers.AddRange(mods);
            return this;
        }

        public Property AddArgument(Argument arg) {
            arguments.Add(arg);
            return this;
        }

        public Property AddArguments(params Argument[] args) {
            arguments.AddRange(args);
            return this;
        }

        public Property AddArguments(IEnumerable<Argument> args) {
            arguments.AddRange(args);
            return this;
        }

        public Property WithComment(string comment) {
            this.comment = comment;
            return this;
        }
    }
}