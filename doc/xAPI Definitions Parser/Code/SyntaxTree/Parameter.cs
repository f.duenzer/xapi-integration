﻿namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A parameter in the syntax tree
    /// </summary>
    public class Parameter {
        private string name;
        private string type;
        private object defVal;
        
        /// <summary>
        /// name of the parameter
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Type of the parameter
        /// </summary>
        public string Type => type;
        /// <summary>
        /// Default value
        /// </summary>
        public object DefVal => defVal;
        
        public Parameter() { }

        public Parameter(string name, string type = default, object defVal = default) {
            this.name = name;
            this.type = type;
            this.defVal = defVal;
        }

        public Parameter WithName(string name) {
            this.name = name;
            return this;
        }

        public Parameter WithType(string type) {
            this.type = type;
            return this;
        }

        public Parameter WithDefVal(object defVal) {
            this.defVal = defVal;
            return this;
        }
    }
}