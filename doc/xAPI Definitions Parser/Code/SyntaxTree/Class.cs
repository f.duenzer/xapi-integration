﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A class in the syntax tree
    /// </summary>
    public class Class {
        private string name;
        private string baseClass;
        private List<Modifier> modifiers = new List<Modifier>();
        private List<Property> properties = new List<Property>();
        private List<Constructor> constructors = new List<Constructor>();
        private List<GetableProperty> getableProperties = new List<GetableProperty>();
        private List<Method> methods = new List<Method>();
        private string comment;

        /// <summary>
        /// Name of the class
        /// </summary>
        public string Name => name;
        /// <summary>
        /// Base class of the class
        /// </summary>
        public string BaseClass => baseClass;
        /// <summary>
        /// Access modifiers of the class
        /// </summary>
        public IReadOnlyList<Modifier> Modifiers => modifiers.AsReadOnly();
        /// <summary>
        /// Properties of the class
        /// </summary>
        public IReadOnlyList<Property> Properties => properties.AsReadOnly();
        /// <summary>
        /// Constructors of the class
        /// </summary>
        public IReadOnlyList<Constructor> Constructors => constructors.AsReadOnly();
        /// <summary>
        /// Properties of the class that have a getter
        /// </summary>
        public IReadOnlyList<GetableProperty> GetableProperties => getableProperties.AsReadOnly();
        /// <summary>
        /// Methods of the class
        /// </summary>
        public IReadOnlyList<Method> Methods => methods.AsReadOnly();
        /// <summary>
        /// Comment of the class
        /// </summary>
        public string Comment => comment;

        public Class() { }

        public Class(string name, string baseClass = default, string comment = default) {
            this.name = name;
            this.baseClass = baseClass;
            this.comment = comment;
        }

        public Class WithName(string name) {
            this.name = name;
            return this;
        }

        public Class WithBaseClass(string baseClass) {
            this.baseClass = baseClass;
            return this;
        }

        public Class AddModifier(Modifier modifier) {
            modifiers.Add(modifier);
            return this;
        }

        public Class AddModifiers(params Modifier[] modifiers) {
            this.modifiers.AddRange(modifiers);
            return this;
        }

        public Class AddModifiers(IEnumerable<Modifier> modifiers) {
            this.modifiers.AddRange(modifiers);
            return this;
        }

        public Class AddProperty(Property property) {
            properties.Add(property);
            return this;
        }

        public Class AddProperties(params Property[] properties) {
            this.properties.AddRange(properties);
            return this;
        }

        public Class AddProperties(IEnumerable<Property> properties) {
            this.properties.AddRange(properties);
            return this;
        }

        public Class AddConstructor(Constructor constructor) {
            constructors.Add(constructor);
            return this;
        }

        public Class AddConstructors(params Constructor[] constructors) {
            this.constructors.AddRange(constructors);
            return this;
        }

        public Class AddConstructors(IEnumerable<Constructor> constructors) {
            this.constructors.AddRange(constructors);
            return this;
        }

        public Class AddGetableProperty(GetableProperty getableProperty) {
            getableProperties.Add(getableProperty);
            return this;
        }

        public Class AddGetableProperties(params GetableProperty[] getableProperties) {
            this.getableProperties.AddRange(getableProperties);
            return this;
        }

        public Class AddGetableProperties(IEnumerable<GetableProperty> getableProperties) {
            this.getableProperties.AddRange(getableProperties);
            return this;
        }

        public Class AddMethod(Method method) {
            methods.Add(method);
            return this;
        }

        public Class AddMethods(params Method[] methods) {
            this.methods.AddRange(methods);
            return this;
        }

        public Class AddMethods(IEnumerable<Method> methods) {
            this.methods.AddRange(methods);
            return this;
        }

        public Class WithComment(string comment) {
            this.comment = comment;
            return this;
        }
    }
}