﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using xAPI_Definitions_Parser.Code.SyntaxTree;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser.Code.CSharp {
    /// <summary>
    /// Generates C# code files from the xAPI definitions
    /// </summary>
    public class CSharpGenerator : CodeGenerator {
        public override CodeLanguage Language => CodeLanguage.CSharp;

        public override string Namespace { get; }

        public override string NamespaceCore { get; }

        public override string NamespaceDefinitions { get; }

        private const string StatementsFile = "xAPI_Statements.cs";

        /// <summary>
        /// Creates a CSharpGenerator
        /// </summary>
        /// <param name="ns">Namespace</param>
        /// <param name="nsCore">Core namespace</param>
        /// <param name="nsDef">Definitions namespace</param>
        public CSharpGenerator(string ns, string nsCore = default, string nsDef = default)
            : base(new CSharpSerializer()) {
            Namespace = ns;
            NamespaceCore = string.IsNullOrEmpty(nsCore) ? Namespace + "." + CoreDirName : nsCore;
            NamespaceDefinitions = string.IsNullOrEmpty(nsDef) ? Namespace + "." + DefinitionsDirName : nsDef;
        }

        protected override List<Import> CreateActivitiesImports(List<string> classes) {
            return new List<Import> {
                new Import("System.Collections.Generic"),
                new Import(NamespaceCore)
            };
        }

        protected override List<Import> CreateVerbsImports(List<string> classes) {
            return new List<Import> {
                new Import("System.Collections.Generic"),
                new Import(NamespaceCore)
            };
        }

        protected override List<Import> CreateExtensionsImports(string type, List<string> classes) {
            return new List<Import> {
                new Import("System.Collections.Generic"),
                new Import(NamespaceCore)
            };
        }

        protected override List<Import> CreateContextImports(xAPIContext context) {
            return new List<Import>() {
                new Import(NamespaceCore)
            };
        }

        protected override List<Import> CreateContextExtensionsImports(xAPIContext context) {
            return new List<Import> {
            };
        }

        protected override List<Import> CreateDefinitionsImports(List<xAPIContext> contexts) {
            return new List<Import> {
                new Import(NamespaceDefinitions)
            };
        }

        protected override List<Import> CreateExtensionTypeImports() {
            return new List<Import> {
                new Import(NamespaceCore)
            };
        }

        protected override MethodBody CreateExtensionsMethodBody(xAPIExtension extension) {
            var args = string.Format("{0}context: Context,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}extensionType: ExtensionType,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}key: \"{1}\",{2}", Indent(2), extension.DefName, Environment.NewLine)
                + string.Format("{0}names: {1},{2}", Indent(2), Serializer.DictionaryToString(extension.Names, 2), Environment.NewLine)
                + string.Format("{0}descriptions: {1}", Indent(2), Serializer.DictionaryToString(extension.Descriptions, 2));
            return new MethodBody()
                .AddStatement(new Statement($"Add(new {ExtensionClassName}({Environment.NewLine}{args}),{Environment.NewLine}{Indent(1)} value);"))
                .AddStatement(new Statement("return this;"));
        }

        protected override MethodBody CreateContextExtensionsGetterBody(xAPIContext context, string type) {
            return new MethodBody()
                .AddStatement(new Statement($"return new {BuildExtensionsClassName(type.Capitalize(), context.Name.Capitalize())}();"));
        }

        public override List<FileMeta> GenerateCoreFiles() {
            var files = base.GenerateCoreFiles();
            var indent = Indent(1);
            foreach (var file in files) {
                var insert = file.Name.EndsWith(StatementsFile) ? $"{indent}using global::{NamespaceDefinitions};{Environment.NewLine}" : string.Empty;
                file.FileContent = $"namespace {NamespaceCore} {{" + Environment.NewLine
                    + insert
                    + indent
                    + file.FileContent.Replace(Environment.NewLine, Environment.NewLine + indent)
                    + Environment.NewLine + "}";
            }
            return files;
        }

        public override CompilationResult Compile(List<FileMeta> coreFiles, List<FileMeta> defFiles, CompilationArguments args) {
            var options = new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary)
                .WithOptimizationLevel(OptimizationLevel.Release)
                .WithPlatform(Platform.AnyCpu);
            var csharpArgs = args as CSharpCompilationArguments;
            var coreVersions = GetNetCoreVersions();
            if (csharpArgs == null) {
                throw new ArgumentException("args not of type CSharpCompilationArguments");
            } else if (coreVersions.Count == 0) {
                throw new Exception("No .NETCore version found");
            } else {
                var trees = new List<Microsoft.CodeAnalysis.SyntaxTree>();
                FileMeta statementsFile = null;
                foreach (var file in coreFiles) {
                    if (file.Name.EndsWith(StatementsFile)) {
                        statementsFile = new FileMeta(
                            file.Name,
                            System.IO.Path.Combine(System.IO.Path.GetDirectoryName(args.Destination), file.Name),
                            file.FileContent);
                    } else {
                        trees.Add(SyntaxFactory.ParseSyntaxTree(file.FileContent, CSharpParseOptions.Default, ""));
                    }
                }
                foreach (var file in defFiles) {
                    trees.Add(SyntaxFactory.ParseSyntaxTree(file.FileContent, CSharpParseOptions.Default, ""));
                }

                string path = null;
                string[] refFiles = new string[] { "System.Collections.dll", "System.Runtime.dll", "System.Linq.dll" };
                if (   !String.IsNullOrEmpty(csharpArgs.RefPath)
                    && DirContainsFiles(csharpArgs.RefPath, refFiles)) {
                    // if refPath not empty and contains required files, take it
                    path = csharpArgs.RefPath;
                } else if (   !String.IsNullOrEmpty(csharpArgs.Dotnet)
                           && DirContainsDotnetFiles(coreVersions, csharpArgs.Dotnet, out path, refFiles)) {
                    // else if dotnet not empty and contains required files, take it
                } else if (DirContainsDotnetFiles(coreVersions, "5.0", out path, refFiles)) {
                    // else if 5.0 as default exists and contains required files, take it
                } else {
                    // else take the first one it finds
                    foreach (var version in coreVersions) {
                        if (DirContainsFiles(System.IO.Path.Combine(version.Value, version.Key), refFiles)) {
                            path = System.IO.Path.Combine(version.Value, version.Key);
                            break;
                        }
                    }
                }

                if (path != null) {
                    for (int i = 0; i < refFiles.Length; ++i) {
                        refFiles[i] = System.IO.Path.Combine(path, refFiles[i]);
                    }

                    var metadataReferences = new MetadataReference[] {
                        MetadataReference.CreateFromFile(refFiles[0]),
                        MetadataReference.CreateFromFile(refFiles[1]),
                        MetadataReference.CreateFromFile(refFiles[2])
                    };

                    var compilation = CSharpCompilation.Create(System.IO.Path.GetFileNameWithoutExtension(csharpArgs.Destination))
                        .WithOptions(options)
                        .AddSyntaxTrees(trees)
                        .AddReferences(metadataReferences);

                    using (var ms = new System.IO.MemoryStream()) {
                        var result = compilation.Emit(ms);
                        ms.Seek(0, System.IO.SeekOrigin.Begin);
                        return new CSharpCompilationResult(true, ms.ToArray(), statementsFile, refFiles);
                    }
                }
            }
            return new CSharpCompilationResult(false);
        }

        private bool DirContainsDotnetFiles(Dictionary<string, string> coreVersions, string dotnet, out string path, params string[] files) {
            string foundPath = null;
            foreach (var version in coreVersions) {
                if (version.Key.StartsWith(dotnet)) {
                    foundPath = version.Value;
                }
            }

            path = foundPath;

            if (path != null) {
                return DirContainsFiles(path, files);
            } else {
                return false;
            }
        }

        private bool DirContainsFiles(string path, params string[] files) {
            foreach (var file in files) {
                if (!System.IO.File.Exists(System.IO.Path.Combine(path, file))) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Searches the system for installed .Net Core versions
        /// </summary>
        /// <returns>.Net Core versions found on the system</returns>
        public static Dictionary<string, string> GetNetCoreVersions() {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "dotnet";
            process.StartInfo.Arguments = "--list-runtimes";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
            var versions = new Dictionary<string, string>();
            string line;
            while (!process.StandardOutput.EndOfStream) {
                line = process.StandardOutput.ReadLine();
                if (line.StartsWith("Microsoft.NETCore.App")) {
                    var parts = line.Substring(21).Split(new char[] { '[', ']' }, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length == 2) {
                        var packsDir = parts[1].Replace("shared", "packs") + ".Ref";
                        if (System.IO.Directory.Exists(packsDir)) {
                            var dirs = System.IO.Directory.GetDirectories(packsDir);
                            foreach (var dir in dirs) {
                                var subDir = System.IO.Path.Combine(dir, "ref");
                                var refDirs = System.IO.Directory.GetDirectories(subDir);
                                var key = System.IO.Path.GetFileName(dir);
                                if (refDirs.Length > 0 && !versions.ContainsKey(key)) {
                                    versions.Add(key, refDirs[0]);
                                }
                            }
                        }
                        //versions.Add(parts[0].Trim(), parts[1].Trim());
                    }
                }
            }
            process.WaitForExit();
            return versions;
        }
    }
}