﻿using System;
using System.Collections.Generic;
using xAPI_Definitions_Parser.Code.SyntaxTree;

namespace xAPI_Definitions_Parser.Code.CSharp {
    /// <summary>
    /// Serializes C# source code out of a syntax tree
    /// </summary>
    public class CSharpSerializer : SyntaxTreeSerializer {
        private IDictionary<Modifier, string> modifiers = new Dictionary<Modifier, string> {
            [Modifier.Public] = "public",
            [Modifier.Private] = "private",
            [Modifier.Protected] = "protected",
            [Modifier.Static] = "static",
            [Modifier.Virtual] = "virtual"
        };

        protected override IDictionary<Modifier, string> Modifiers => modifiers;

        public override string DictionaryToString(IDictionary<string, string> dict, int indentDepth) {
            var str = $"new Dictionary<string, string> {{{Environment.NewLine}";
            int count = 0;
            if (dict != null) {
                foreach (var pair in dict) {
                    ++count;
                    str += string.Format("{0}[\"{1}\"] = \"{2}\"{3}",
                        CodeGenerator.Indent(indentDepth + 1),
                        pair.Key,
                        pair.Value.ClearString(),
                        count == dict.Count ? string.Empty : $",{Environment.NewLine}");
                }
            }
            str += " }";
            return str;
        }

        public override string RootToString(Root root) {
            var indent = 0;
            var formatStr = "{0}";
            if (!string.IsNullOrEmpty(root?.Namespace)) {
                formatStr = NamespaceToString(root?.Namespace);
                ++indent;
            }

            var importStr = string.Empty;
            if (root?.Imports != null) {
                foreach (var import in root.Imports) {
                    importStr += ImportToString(import, indent);
                }
            }
            importStr += Environment.NewLine;

            var classStr = ClassToString(root?.Class, indent);

            return string.Format(formatStr, importStr + classStr);
        }

        private string NamespaceToString(string ns) {
            return $"namespace {ns} {{{{{Environment.NewLine}{{0}}{Environment.NewLine}}}}}";
        }

        protected override string ImportToString(Import import, int indentDepth) {
            return $"{CodeGenerator.Indent(indentDepth)}using {import?.Target};{Environment.NewLine}";
        }

        protected override string ClassToString(Class cls, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var baseStr = string.IsNullOrEmpty(cls?.BaseClass) ? "" : $" : {cls?.BaseClass}";
            var classStr = $"{indentStr}{ModifiersToString(cls?.Modifiers)}class {cls?.Name}{baseStr} {{{Environment.NewLine}" +
                ClassContentsToString(cls, indentDepth + 1) +
                $"{indentStr}}}";
            if (!string.IsNullOrEmpty(cls?.Comment)) {
                classStr = CommentToString(cls.Comment, indentDepth) + classStr;
            }
            return classStr;
        }

        protected override string GetablePropertyToString(GetableProperty property, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var indentStr2 = CodeGenerator.Indent(indentDepth + 1);
            return $"{indentStr}{ModifiersToString(property?.Modifiers)}{property?.Type} {property?.Name} {{{Environment.NewLine}" +
                $"{indentStr2}get {{{Environment.NewLine}" +
                MethodBodyToString(property?.Getter?.Body, indentDepth + 2) +
                $"{indentStr2}}}{Environment.NewLine}" +
                $"{indentStr}}}{Environment.NewLine}";
        }

        protected override string PropertyToString(Property property, int indentDepth) {
            var argsStr = ArgumentsToString(property?.Arguments, indentDepth + 1, true, true);
            if (argsStr != "") {
                argsStr = $"{Environment.NewLine}{CodeGenerator.Indent(indentDepth + 1)}{argsStr}";
            }
            var propStr = $"{CodeGenerator.Indent(indentDepth)}{ModifiersToString(property?.Modifiers)}{property?.Type} {property?.Name} = " +
                $"new {property?.Type}({argsStr});{Environment.NewLine}";
            if (!string.IsNullOrEmpty(property?.Comment)) {
                propStr = PropertyCommentToString(property, indentDepth) + propStr;
            }
            return propStr;
        }

        protected override string ConstructorToString(Constructor constructor, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var baseStr = constructor?.BaseArguments == null || constructor?.BaseArguments.Count == 0 ?
                "" :
                $"{Environment.NewLine}{CodeGenerator.Indent(indentDepth + 1)}: base({ArgumentsToString(constructor?.BaseArguments, indentDepth + 1)}) ";
            return $"{indentStr}{ModifiersToString(constructor?.Modifiers)}{constructor?.Type}({ParametersToString(constructor?.Parameters)}) " +
                $"{baseStr}" +
                $"{{{Environment.NewLine}{indentStr}}}{Environment.NewLine}";
        }

        protected override string MethodToString(Method method, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            return $"{indentStr}{ModifiersToString(method?.Modifiers)}{method?.Type} {method?.Name}({ParametersToString(method?.Parameters)}) {{{Environment.NewLine}" +
                $"{MethodBodyToString(method?.Body, indentDepth + 1)}{indentStr}}}{Environment.NewLine}";
        }

        protected override string ArgumentsToString(IReadOnlyList<Argument> args, int indentDepth, bool newLines = false, bool withName = false) {
            var str = string.Empty;
            if (args != null) {
                for (int i = 0; i < args.Count; ++i) {
                    if (withName) {
                        str += args[i].Name + ": ";
                    }
                    str += args[i].Value is Dictionary<string, string> dict ? DictionaryToString(dict, indentDepth)
                        : (args[i].Value is Null n ? NullToString(n) : args[i].Value);
                    if (i < args.Count - 1) {
                        str += $",{(newLines ? Environment.NewLine + CodeGenerator.Indent(indentDepth) : " ")}";
                    }
                }
            }
            return str;
        }

        protected override string ParametersToString(IReadOnlyList<Parameter> parameters) {
            var str = string.Empty;
            if (parameters != null) {
                for (int i = 0; i < parameters.Count; ++i) {
                    str += $"{parameters[i].Type} {parameters[i].Name}";
                    if (parameters[i].DefVal != null) {
                        str += $" = {(parameters[i].DefVal is Null n ? NullToString(n) : parameters[i].DefVal)}";
                    }
                    if (i < parameters.Count - 1) {
                        str += ", ";
                    }
                }
            }
            return str;
        }

        protected override string PropertyCommentToString(Property property, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var str = $"{indentStr}/// <summary>{Environment.NewLine}";
#if NET5_0
            var commentLines = property?.Comment.Split(Environment.NewLine);
#elif NETSTANDARD2_0
            var commentLines = property?.Comment.Split(Environment.NewLine.ToCharArray());
#endif
            if (commentLines != null) {
                foreach (var line in commentLines) {
                    str += $"{indentStr}/// {line}{Environment.NewLine}";
                }
            }
            str += $"{indentStr}/// </summary>{Environment.NewLine}";
            return str;
        }

        protected override string CommentToString(string comment, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var str = $"{indentStr}/// <summary>{Environment.NewLine}";
#if NET5_0
            var commentLines = comment?.Split(Environment.NewLine);
#elif NETSTANDARD2_0
            var commentLines = comment?.Split(Environment.NewLine.ToCharArray());
#endif
            if (commentLines != null) {
                foreach (var line in commentLines) {
                    str += $"{indentStr}/// {line}{Environment.NewLine}";
                }
            }
            str += $"{indentStr}/// </summary>{Environment.NewLine}";
            return str;
        }
    }
}