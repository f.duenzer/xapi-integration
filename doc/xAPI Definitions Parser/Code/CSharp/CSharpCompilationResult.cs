﻿using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser.Code.CSharp {
    public class CSharpCompilationResult : CompilationResult {
        /// <summary>
        /// Compiled DLL as a byte array
        /// </summary>
        public byte[] Compiled { get; }

        public FileMeta StatementsFile { get; }

        public string[] RefFiles { get; }

        public CSharpCompilationResult(bool success, byte[] compiled = null, FileMeta statementsFile = null, string[] refFiles = null) : base(success) {
            Compiled = compiled;
            StatementsFile = statementsFile;
            RefFiles = refFiles;
        }
    }
}