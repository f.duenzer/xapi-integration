﻿namespace xAPI_Definitions_Parser.Code.CSharp {
    /// <summary>
    /// Contains arguments for the CSharpCodeGenerator's compile method
    /// </summary>
    public class CSharpCompilationArguments : CompilationArguments {
        /// <summary>
        /// .Net version to compile to
        /// </summary>
        public string Dotnet { get; }
        /// <summary>
        /// Directory that the DLLs to be used are in
        /// </summary>
        public string RefPath { get; }

        public CSharpCompilationArguments(string dest, string dotnet = default, string refPath = default)
            : base(CodeLanguage.CSharp, dest) {
            Dotnet = dotnet;
            RefPath = refPath;
        }
    }
}