﻿using System;
using System.Collections.Generic;
using xAPI_Definitions_Parser.Code.SyntaxTree;

namespace xAPI_Definitions_Parser.Code.Python {
    /// <summary>
    /// Serializes Python source code out of a syntax tree
    /// </summary>
    public class PythonSerializer : SyntaxTreeSerializer {
        private IDictionary<Modifier, string> modifiers = new Dictionary<Modifier, string> {
        };

        protected override IDictionary<Modifier, string> Modifiers => modifiers;

        public override string DictionaryToString(IDictionary<string, string> dict, int indentDepth) {
            var str = $"{{{Environment.NewLine}";
            int count = 0;
            if (dict != null) {
                foreach (var pair in dict) {
                    ++count;
                    str += string.Format("{0}\"{1}\": \"{2}\"{3}{4}",
                        CodeGenerator.Indent(indentDepth + 1),
                        pair.Key,
                        pair.Value.ClearString(),
                        count == dict.Count ? string.Empty : ",",
                        Environment.NewLine);
                }
            }
            str += $"{CodeGenerator.Indent(indentDepth)}}}";
            return str;
        }

        public override string RootToString(Root root) {
            var importStr = string.Empty;
            if (root != null) {
                foreach (var import in root.Imports) {
                    importStr += ImportToString(import, 0);
                }
                importStr += Environment.NewLine;

                importStr += ClassToString(root.Class, 0);
            }

            return importStr;
        }

        public override string NullToString(Null n) {
            return "None";
        }

        protected override string ImportToString(Import import, int indentDepth) {
            return $"{CodeGenerator.Indent(indentDepth)}from {import?.Source} import {import?.Target}{Environment.NewLine}";
        }
        
        protected override string ClassToString(Class cls, int indentDepth) {
            var baseStr = string.IsNullOrEmpty(cls?.BaseClass) ? "" : $"({cls?.BaseClass})";
            var classStr = $"class {cls?.Name}{baseStr}:{Environment.NewLine}" +
                ClassContentsToString(cls, indentDepth + 1);
            if (!string.IsNullOrEmpty(cls?.Comment)) {
                classStr = CommentToString(cls.Comment, indentDepth) + classStr;
            }
            return classStr;
        }
        
        protected override string GetablePropertyToString(GetableProperty property, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            return $"{indentStr}@property{Environment.NewLine}" +
                $"{indentStr}def {property?.Name}(self):{Environment.NewLine}" +
                MethodBodyToString(property?.Getter?.Body, indentDepth + 1);
        }
        
        protected override string PropertyToString(Property property, int indentDepth) {
            var argsStr = ArgumentsToString(property?.Arguments, indentDepth + 1, true);
            if (argsStr != "") {
                argsStr = $"{Environment.NewLine}{CodeGenerator.Indent(indentDepth + 1)}{argsStr}";
            }
            var propStr = $"{CodeGenerator.Indent(indentDepth)}{property?.Name} = " +
                $"{property?.Type}({argsStr}){Environment.NewLine}";
            if (!string.IsNullOrEmpty(property?.Comment)) {
                propStr += PropertyCommentToString(property, indentDepth);
            }
            return propStr;
        }
        
        protected override string ConstructorToString(Constructor constructor, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var bodyStr = constructor?.BaseArguments == null || constructor?.BaseArguments.Count == 0 ?
                $"{CodeGenerator.Indent(indentDepth + 1)}pass{Environment.NewLine}" :
                $"{CodeGenerator.Indent(indentDepth + 1)}super().__init__({ArgumentsToString(constructor?.BaseArguments, indentDepth + 1)}){Environment.NewLine}";
            return $"{indentStr}def __init__({ParametersToString(constructor?.Parameters)}):{Environment.NewLine}" +
                $"{bodyStr}";
        }
        
        protected override string MethodToString(Method method, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            return $"{indentStr}def {method?.Name}({ParametersToString(method?.Parameters)}):{Environment.NewLine}" +
                $"{MethodBodyToString(method?.Body, indentDepth + 1)}";
        }

        protected override string ArgumentsToString(IReadOnlyList<Argument> args, int indentDepth, bool newLines = false, bool withName = false) {
            var str = string.Empty;
            if (args != null) {
                for (int i = 0; i < args.Count; ++i) {
                    str += args[i].Value is Dictionary<string, string> dict ? DictionaryToString(dict, indentDepth)
                        : (args[i].Value is Null n ? NullToString(n) : args[i].Value);
                    if (i < args.Count - 1) {
                        str += $",{(newLines ? Environment.NewLine + CodeGenerator.Indent(indentDepth) : " ")}";
                    }
                }
            }
            return str;
        }

        protected override string ParametersToString(IReadOnlyList<Parameter> parameters) {
            var str = "self";
            if (parameters != null) {
                foreach (var parameter in parameters) {
                    str += ", " + parameter.Name;
                    if (parameter.DefVal != null) {
                        str += $" = {(parameter.DefVal is Null n ? NullToString(n) : parameter.DefVal)}";
                    }
                }
            }
            return str;
        }

        protected override string PropertyCommentToString(Property property, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var str = $"{indentStr}\"\"\"";
#if NET5_0
            var commentLines = property?.Comment.Split(Environment.NewLine);
#elif NETSTANDARD2_0
            var commentLines = property?.Comment.Split(Environment.NewLine.ToCharArray());
#endif
            if (commentLines != null && commentLines.Length > 0) {
                str += commentLines[0];
                for (int i = 1; i < commentLines.Length; ++i) {
                    str += $"{Environment.NewLine}{indentStr}{commentLines[i]}";
                }
            }
            str += $"\"\"\"{Environment.NewLine}";
            return str;
        }

        protected override string CommentToString(string comment, int indentDepth) {
            var indentStr = CodeGenerator.Indent(indentDepth);
            var str = $"{indentStr}\"\"\"";
#if NET5_0
            var commentLines = comment?.Split(Environment.NewLine);
#elif NETSTANDARD2_0
            var commentLines = comment?.Split(Environment.NewLine.ToCharArray());
#endif
            if (commentLines != null && commentLines.Length > 0) {
                str += commentLines[0];
                for (int i = 1; i < commentLines.Length; ++i) {
                    str += $"{Environment.NewLine}{indentStr}{commentLines[i]}";
                }
            }
            str += $"\"\"\"{Environment.NewLine}";
            return str;
        }
    }
}