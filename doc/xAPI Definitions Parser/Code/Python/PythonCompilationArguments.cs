﻿namespace xAPI_Definitions_Parser.Code.Python {
    /// <summary>
    /// Contains arguments for the PythonCodeGenerator's compile method
    /// </summary>
    public class PythonCompilationArguments : CompilationArguments {
        public PythonCompilationArguments(string dest)
            : base(CodeLanguage.Python, dest) {

        }
    }
}