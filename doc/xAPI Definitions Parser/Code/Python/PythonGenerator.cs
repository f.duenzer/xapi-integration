﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using xAPI_Definitions_Parser.Code.SyntaxTree;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser.Code.Python {
    /// <summary>
    /// Generates Python code files from the xAPI definitions
    /// </summary>
    public class PythonGenerator : CodeGenerator {
        public override string CoreDirName => base.CoreDirName.ToLower();
        public override string DefinitionsDirName => base.DefinitionsDirName.ToLower();

        public override CodeLanguage Language => CodeLanguage.Python;

        public override string Namespace => default;

        public override string NamespaceCore => default;

        public override string NamespaceDefinitions => default;

        /// <summary>
        /// Creates a PythonGenerator
        /// </summary>
        public PythonGenerator()
            : base(new PythonSerializer()) { }

        protected override string CreateFileName(string name) => base.CreateFileName(name).ToLower();

        protected override List<Import> CreateActivitiesImports(List<string> classes) {
            var imports = new List<Import> {
                new Import(ActivityClassName, $"..{CoreDirName}.{ActivityClassName.ToLower()}"),
                new Import(ActivitiesClassName, $"..{CoreDirName}.{ActivitiesClassName.ToLower()}")
            };

            foreach (var cls in classes) {
                imports.Add(new Import(cls, $".{cls.ToLower()}"));
            }

            return imports;
        }
        
        protected override List<Import> CreateVerbsImports(List<string> classes) {
            var imports = new List<Import> {
                new Import(VerbClassName, $"..{CoreDirName}.{VerbClassName.ToLower()}"),
                new Import(VerbsClassName, $"..{CoreDirName}.{VerbsClassName.ToLower()}")
            };

            foreach (var cls in classes) {
                imports.Add(new Import(cls, $".{cls.ToLower()}"));
            }

            return imports;
        }
        
        protected override List<Import> CreateExtensionsImports(string type, List<string> classes) {
            var importName = BuildExtensionTypeClassName(type.Capitalize());
            var imports = new List<Import> {
                new Import(
                    importName,
                    $".{importName.ToLower()}"),
                new Import(
                    ExtensionClassName,
                    $"..{CoreDirName}.{ExtensionClassName.ToLower()}")
            };

            foreach(var cls in classes) {
                imports.Add(new Import(cls, $".{cls.ToLower()}"));
            }

            return imports;
        }
        
        protected override List<Import> CreateContextImports(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var verbsName = BuildVerbsClassName(cxtCap);
            var activitiesName = BuildActivitiesClassName(cxtCap);
            var contextExtensionsName = BuildContextExtensionsClassName(cxtCap);
            return new List<Import> {
                new Import(ContextClassName, $"..{CoreDirName}.{ContextClassName.ToLower()}"),
                new Import(verbsName, $".{verbsName.ToLower()}"),
                new Import(activitiesName, $".{activitiesName.ToLower()}"),
                new Import(contextExtensionsName, $".{contextExtensionsName.ToLower()}")
            };
        }
        
        protected override List<Import> CreateContextExtensionsImports(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var imports = new List<Import>();
            foreach (var extType in context.ExtensionTypes) {
                var extensionsName = BuildExtensionsClassName(extType.Capitalize(), cxtCap);
                imports.Add(new Import(
                    extensionsName,
                    $".{extensionsName.ToLower()}"));
            }
            return imports;
        }
        
        protected override List<Import> CreateDefinitionsImports(List<xAPIContext> contexts) {
            var imports = new List<Import>();
            foreach (var context in contexts) {
                var contextName = BuildContextClassName(context.Name.Capitalize());
                imports.Add(new Import(
                    contextName,
                    $".{contextName.ToLower()}"));
            }
            return imports;
        }
        
        protected override List<Import> CreateExtensionTypeImports() {
            return new List<Import> {
                new Import(ExtensionsClassName, $"..{CoreDirName}.{ExtensionsClassName.ToLower()}")
            };
        }
        
        protected override MethodBody CreateExtensionsMethodBody(xAPIExtension extension) {
            var args = string.Format("{0}self.context,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}self.extensionType,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}\"{1}\",{2}", Indent(2), extension.DefName, Environment.NewLine)
                + string.Format("{0}{1},{2}", Indent(2), Serializer.DictionaryToString(extension.Names, 2), Environment.NewLine)
                + string.Format("{0}{1}", Indent(2), Serializer.DictionaryToString(extension.Descriptions, 2));
            return new MethodBody()
                .AddStatement(new Statement($"self.add({ExtensionClassName}({Environment.NewLine}{args}),{Environment.NewLine}{Indent(1)}value);"))
                .AddStatement(new Statement("return self;"));
        }

        protected override MethodBody CreateContextExtensionsGetterBody(xAPIContext context, string type) {
            return new MethodBody()
                .AddStatement(new Statement($"return {BuildExtensionsClassName(type.Capitalize(), context.Name.Capitalize())}();"));
        }

        public override FileMeta GenerateExtensionTypeFile(string type) {
            var name = BuildExtensionTypeClassName(type.Capitalize());
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var cls = new Class(name, ExtensionsClassName)
                .AddModifier(Modifier.Public)
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddParameter(new Parameter("context", "string", new Null()))
                    .AddBaseArgument(new Argument($"\"{type}\""))
                    .AddBaseArgument(new Argument("context")));
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateExtensionTypeImports());
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        public override List<FileMeta> GenerateContexts(List<xAPIContext> contexts) {
            var files = base.GenerateContexts(contexts);
            files.Add(new FileMeta("__init__.py", ""));
            return files;
        }

        public override CompilationResult Compile(List<FileMeta> coreFiles, List<FileMeta> defFiles, CompilationArguments args) {
            var setup = $"from setuptools import setup{Environment.NewLine}" +
                Environment.NewLine +
                $"setup({Environment.NewLine}" +
                $"{Indent(1)}name='xAPI',{Environment.NewLine}" +
                $"{Indent(1)}version='1.0.0',{Environment.NewLine}" +
                $"{Indent(1)}packages=['core', 'definitions']{Environment.NewLine}" +
                ")";
            System.IO.File.WriteAllText(System.IO.Path.Combine(args.Destination, "setup.py"), setup);
            Process process = new Process();
            process.StartInfo.WorkingDirectory = args.Destination;
            process.StartInfo.FileName = "python";
            process.StartInfo.Arguments = "setup.py sdist bdist_wheel";
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            process.WaitForExit();
            return new PythonCompilationResult(true);
        }
    }
}