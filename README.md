# xAPI Definitions Parser

The API and tools in this package allow for the generation of static C#, Python and JavaScript classes for xAPI definitions in the format of the RWTH Learning Technologies Research Group's vocabulary (https://gitlab.com/learntech-rwth/xapi) to make them immediately usable in the target language.
The statements must then only be passed to the LRS controller that you are using. There are four ways of generating these classes:

## DLL

The core functionality is provided as a .NET Standard 2.0 DLL. The DLL is useful for you, if you want to build another tool around this functionality or want access to inbetween steps. Here is a quick overview of the components and their interaction:  
  
- Generator: static class that provides single methods to execute the full generation of xAPI classes without worrying about the details
- IOManager: abstract class to read input and write output, reading creates FileMeta or Context objects, writing takes FileMeta objects. There are two implementations of this class:  
	- FileManager (local files)  
	- GitLabManager (GitLab files)  
- Deserializer: static class that takes the objects created by the IOManager and deserializes the JSON content to internal definitions classes  
- CodeGenerator: abstract class to generate code files, a library or package out of the definitions classes. The generated FileMeta objects can then be written by an IOManager. There are three implementations of this class for their corresponding languages:  
	- CSharpGenerator  
	- PythonGenerator  
	- JavaScriptGenerator  

An HTML documentation for the API can be found in the doc folder. The entry document is index.html.

## xAPI Definitions Parser CLI

This is a console app to use the core DLL to generate xAPI classes. It takes these arguments:  
<pre>
-source 	Type of source to read from (local, gitlab)  
-path		Definitions path to read from (i.e. C:\path\definitions for local or definitions for gitlab)  
-lang		Language to generate classes for (CSharp, Python, JavaScript)  
-namespace	Namespace of the generated classes (only relevant for CSharp)  
-output		Local path to write generated classes to  
-compile	Destination to write compilation/package output to (for C# the full dll file name, for Python the directory)  
-dotnet		For C# .NET version to compile to (must be installed on the system)  
-refpath	Alternative to -dotnet, folder to use for compilation that must contain "System.Private.CoreLib.dll", "System.Runtime.dll" and "System.Linq.dll"  
</pre>

## xAPI Definitions WebAPI

The Docker image xapiwebapiimage.tar contains the web API that allows general access to the core functionality. It is also used by the Visual Studio Extension when available to make access to the learntech xAPI repository quicker.  
  
To start it, it must be loaded and then run, mapping the local port 44332 (if it is mapped to a different port and you want to use the extension optimally, it must be changed in the xAPI_WebAPI_Methods.dll.config in the extension directory) to the container port 80. To do so, you can use these two commands:  
  
docker load --input xapiwebapiimage.tar  
docker run -d -p 44332:80 --name xapiwebapicontainer xapiwebapiimage 

If you want to create the container with other settings like a different port there is a Dockerfile in the xAPI Definitions WebAPI project folder.
  
## xAPI VS Extension

This Visual Studio extension integrates the generation into the IDE with a user interface to combine and select definitions from different sources and directly add them to an open project. To install, compile the "xAPI VS Extension" project and run the "xAPI VS Extension.vsix". It is then found in Visual Studio under View -> Other Windows -> xAPI VS Extension. It supports VS Pro and Community 2017 and 2019.

By default the extension loads the learntech-rwth/xapi vocabulary. If the web API cannot be reached this takes a few seconds.

If you want to add other vocabularies, you can do so by filling out the source configuration at the bottom left of the extension and then clicking "Add Source".

When all vocabularies you want to create classes for have been added, you can select the contexts and definitions you want to use from them in the corresponding treeviews at the top.

Once your selection is finished, you can continue to the output configuration where you set language, output type and the project to add the files to. You can select any project in the currently opened solution and select it by selecting its directory. The browse folder dialog can help with that. If everything is properly configured, you can click the "Generate" button and the files will be added to the specified project.

## Generated classes

To use the classes, the generated source code, DLL or Python package must be included. Also, you need the respective programming language's version of the TinCan API. If you are using the classes in C#, you also need Newtonsoft.Json.

The definitions are found in the class "Definitions/xAPI_Definitions". To create actors or bodies use the classes provided in the created "Core" folder. To create TinCan statements to send to an LRS with TinCan, you use "Core/xAPI_Statements" and one of the three "CreateStatement" methods.