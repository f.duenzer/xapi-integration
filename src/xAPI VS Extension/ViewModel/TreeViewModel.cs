﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System.Collections.ObjectModel;

namespace xAPI_VS_Extension.ViewModel {
    public class TreeViewModel : ObservableObject {
        private string name;

        private ObservableCollection<SourceViewModel> sources;

        public string Name {
            get => name;
            set => SetProperty(ref name, value);
        }

        public ObservableCollection<SourceViewModel> Sources {
            get => sources;
            set => SetProperty(ref sources, value);
        }

        public TreeViewModel(string name = default) {
            this.name = name;
            sources = new ObservableCollection<SourceViewModel>();
        }
    }
}