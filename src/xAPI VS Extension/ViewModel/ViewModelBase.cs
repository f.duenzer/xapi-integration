﻿using Microsoft.Toolkit.Mvvm.ComponentModel;

namespace xAPI_VS_Extension.ViewModel {
    public class ViewModelBase : ObservableObject {
        protected bool? isChecked;

        public virtual bool? IsChecked {
            get => isChecked;
            set => SetProperty(ref isChecked, value);
        }

        public ViewModelBase() {
            isChecked = true;
        }
    }
}