﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using xAPI_Definitions_Parser;
using xAPI_Definitions_Parser.IO;

namespace xAPI_VS_Extension.ViewModel {
    public class SourceViewModel : CheckableViewModel {
        private SourceType? type;
        private string path;
        private string projectId;
        private string privateKey;
        private string branch;

        public SourceType? Type {
            get => type;
            set => SetProperty(ref type, value);
        }

        public string Path {
            get => path;
            set => SetProperty(ref path, value);
        }

        public string ProjectId {
            get => projectId;
            set => SetProperty(ref projectId, value);
        }

        public string PrivateKey {
            get => privateKey;
            set => SetProperty(ref privateKey, value);
        }

        public string Branch {
            get => branch;
            set => SetProperty(ref branch, value);
        }

        public SourceViewModel(SourceType? sourceType, string name, string path, string projectId,
            string privateKey, string branch, List<Context> contexts, bool isExpanded = true) : base(name) {
            type = sourceType;
            this.path = path;
            this.projectId = projectId;
            this.privateKey = privateKey;
            this.branch = branch;

            contexts.Sort(new System.Comparison<Context>((c, c2) => c.Name.CompareTo(c2.Name)));

            foreach (var context in contexts) {
                AddChild(new CheckableViewModel(context));
            }

            IsExpanded = isExpanded;
        }

        public List<Context> ToContexts() {
            var contexts = new List<Context>();
            foreach (var child in Children) {
                if (child.IsChecked != false) {
                    var context = new Context(child.Name);
                    foreach (var defs in child.Children) {
                        if (defs.IsChecked != false) {
                            if (defs.Name == MainViewModel.ActivitiesItemString) {
                                context.Activities.AddRange(ToFileMetas(defs.Children,
                                    System.IO.Path.Combine(path ?? string.Empty, context.Name, MainViewModel.ActivitiesItemString)));
                            } else if (defs.Name == MainViewModel.ExtensionsItemString) {
                                foreach (var extTypeDefs in defs.Children) {
                                    if (extTypeDefs.IsChecked != false) {
                                        if (!context.Extensions.ContainsKey(extTypeDefs.Name)) {
                                            context.Extensions.Add(new KeyValuePair<string, List<FileMeta>>(extTypeDefs.Name, new List<FileMeta>()));
                                        }
                                        context.Extensions[extTypeDefs.Name].AddRange(ToFileMetas(extTypeDefs.Children,
                                            System.IO.Path.Combine(path ?? string.Empty, context.Name, MainViewModel.ExtensionsItemString, extTypeDefs.Name)));
                                    }
                                }
                            } else if (defs.Name == MainViewModel.VerbsItemString) {
                                context.Verbs.AddRange(ToFileMetas(defs.Children,
                                    System.IO.Path.Combine(path ?? string.Empty, context.Name, MainViewModel.VerbsItemString)));
                            }
                        }
                    }
                    contexts.Add(context);
                }
            }
            return contexts;
        }

        private List<FileMeta> ToFileMetas(ObservableCollection<CheckableViewModel> vms, string defsPath) {
            var files = new List<FileMeta>();
            foreach (var vm in vms) {
                if (vm.IsChecked != false) {
                    var defPath = System.IO.Path.Combine(defsPath, vm.Name);
                    if (type.HasValue && type.Value == SourceType.GitLab) {
                        defPath = defPath.Replace('\\', '/');
                    }
                    if (vm.Children.Count > 0) {
                        files.Add(new FileMeta(vm.Name.ClearName(), defPath, ToFileMetas(vm.Children, defPath)));
                    } else {
                        files.Add(new FileMeta(vm.Name.ClearName(), defPath + ".json"));
                    }
                }
            }
            return files;
        }
    }
}