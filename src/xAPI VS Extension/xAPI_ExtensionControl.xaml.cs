﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using xAPI_VS_Extension.ViewModel;

namespace xAPI_VS_Extension {
    /// <summary>
    /// Interaction logic for xAPI_ExtensionControl.
    /// </summary>
    public partial class xAPI_ExtensionControl : UserControl {
        private MainViewModel viewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="xAPI_IntegratorControl"/> class.
        /// </summary>
        public xAPI_ExtensionControl() {
            viewModel = new MainViewModel();
            DataContext = viewModel;
            InitializeComponent();
        }

        private void BrowsePathBtn_Click(object sender, RoutedEventArgs e) {
            var dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok) {
                viewModel.Path = dialog.FileName;
            }
        }

        private void BrowseProjectBtn_Click(object sender, RoutedEventArgs e) {
            try {
                ThreadHelper.ThrowIfNotOnUIThread();
                var dialog = new CommonOpenFileDialog();
                var dte = (DTE)Package.GetGlobalService(typeof(DTE));
                if (dte.Solution.Count > 0) {
                    dialog.InitialDirectory = Path.GetDirectoryName(dte.Solution.FullName);
                }
                dialog.IsFolderPicker = true;
                if (dialog.ShowDialog() == CommonFileDialogResult.Ok) {
                    viewModel.Project = Path.GetFileName(dialog.FileName);
                }
            }
            catch (Exception ex) { }
        }

        private void TreeViewMenuItemRemove_Click(object sender, RoutedEventArgs e) {
            viewModel.RemoveTree(((MenuItem)sender).DataContext as TreeViewModel);
        }

        private void TreeView_PreviewMouseWheel(object sender, MouseWheelEventArgs e) {
            if (Keyboard.Modifiers == ModifierKeys.Shift) {
                ScrollViewer.ScrollToHorizontalOffset(ScrollViewer.HorizontalOffset - e.Delta);
                e.Handled = true;
            }
        }
    }
}