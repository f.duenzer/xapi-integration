﻿using System.Collections.Generic;

public class xAPI_Verb : xAPI_Definition {
    public xAPI_Verb(string context, string key, Dictionary<string, string> names, Dictionary<string, string> descriptions)
        : base(context, key, names, descriptions) { }

    public override string GetPath() => $"/{Context}/verbs/{Key}";
}