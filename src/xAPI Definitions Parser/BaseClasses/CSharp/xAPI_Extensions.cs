﻿using System.Collections.Generic;

public abstract class xAPI_Extensions : List<KeyValuePair<xAPI_Extension, object>> {
    public string Context { get; }
    public string ExtensionType { get; }
    public xAPI_Extensions(string extensionType, string context) {
        Context = context;
        ExtensionType = extensionType;
    }

    public xAPI_Extensions Add(xAPI_Extension key, object value) {
        if (key == null)
            return this;

        Add(new KeyValuePair<xAPI_Extension, object>(key, value));
        return this;
    }

    public xAPI_Extensions Remove(xAPI_Extension key) {
        return Remove(key.Key);
    }

    public xAPI_Extensions Add(params xAPI_Extensions[] extensions) {
        foreach (var ext in extensions)
            Add(ext);
        return this;
    }

    public xAPI_Extensions Add(xAPI_Extensions extensions) {
        foreach (var pair in extensions)
            Add(pair.Key, pair.Value);
        return this;
    }

    public xAPI_Extensions Remove(string key) {
        var index = FindIndex(p => p.Key.Key == key);
        base.RemoveAt(index);
        return this;
    }

    public new xAPI_Extensions Clear() {
        base.Clear();
        return this;
    }
}