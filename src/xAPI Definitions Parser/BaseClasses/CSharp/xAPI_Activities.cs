﻿public abstract class xAPI_Activities {
    public string ContextName { get; }

    public xAPI_Activities(string contextName) {
        ContextName = contextName;
    }
}