﻿from ..definitions.xapi_extensions_activity import xAPI_Extensions_Activity
from ..definitions.xapi_extensions_context import xAPI_Extensions_Context
from ..definitions.xapi_extensions_result import xAPI_Extensions_Result
import tincan
from datetime import datetime

class xAPI_Statements:
    @staticmethod
    def __createAgent(actor):
        agent = tincan.Agent()
        agent.name = actor.name
        agent.mbox = actor.email
        return agent

    @staticmethod
    def __createVerb(uri, verb):
        v = tincan.Verb()
        v.id = verb.createValidId(uri)
        v.display = tincan.LanguageMap(verb.names)
        return v

    @staticmethod
    def __createActivity(uri, activity, extensions = None):
        a = tincan.Activity()
        a.id = activity.createValidId(uri)
        a.definition = xAPI_Statements.toTinCanActivityDefinition(activity)
        a.definition.extensions = None if extensions == None else xAPI_Statements.toTinCanExtensions(extensions, uri)
        return a

    @staticmethod
    def splitExtensions(extensions):
        if extensions == None:
            return (None, None, None)

        activityExtensions = xAPI_Extensions_Activity()
        contextExtensions = xAPI_Extensions_Context()
        resultExtensions = xAPI_Extensions_Result()

        for ext in extensions.extensions:
            if activityExtensions.extensionType == ext[0].extensionType:
                activityExtensions.addPair(ext)
            if contextExtensions.extensionType == ext[0].extensionType:
                contextExtensions.addPair(ext)
            if resultExtensions.extensionType == ext[0].extensionType:
                resultExtensions.addPair(ext)

        return (activityExtensions, contextExtensions, resultExtensions)

    @staticmethod
    def createStatement(
        uri,
        actor,
        verb,
        activity,
        activityExtensions = None,
        contextExtensions = None,
        resultExtensions = None,
        score = None,
        completion = True,
        success = True,
        instructor = None):
        s = tincan.Statement()
        s.actor = xAPI_Statements.__createAgent(actor)
        s.verb = xAPI_Statements.__createVerb(uri, verb)
        s.object = xAPI_Statements.__createActivity(uri, activity, activityExtensions)
        s.context = xAPI_Statements.__createContext(uri, contextExtensions, instructor)
        s.result = xAPI_Statements.createResult(uri, score, completion, success, resultExtensions)
        s.timestamp = datetime.now()
        return s

    @staticmethod
    def __createContext(uri, extensions = None, instructor = None):
        c = tincan.Context()
        c.instructor = None if instructor == None else xAPI_Statements.createAgent(instructor)
        c.extensions = None if extensions == None else xAPI_Statements.toTinCanExtensions(extensions, uri)
        return c

    @staticmethod
    def createResult(uri, score = None, completion = None, success = None, extensions = None):
        r = tincan.Result()
        r.score = score
        r.completion = completion
        r.success = success
        r.extensions = None if extensions == None else xAPI_Statements.toTinCanExtensions(extensions, uri)
        return r

    @staticmethod
    def createStatementExt(uri, actor, verb, activity, extensions = None, score = None, completion = True, success = True):
        allExtensions = xAPI_Statements.splitExtensions(extensions)
        return xAPI_Statements.createStatement(uri, actor, verb, activity, allExtensions[0], allExtensions[1], allExtensions[2], score, completion, success)

    @staticmethod
    def createStatementBody(uri, actor, body):
        score = tincan.Score()
        score.max = body.maxScore
        score.min = body.minScore
        score.raw = body.rawScore
        score.scaled = body.scaledScore
        result = xAPI_Statements.createResult(
            uri,
            score,
            body.completion,
            body.success,
            body.resultExtensions
            )
        s = tincan.Statement()
        s.actor = xAPI_Statements.__createAgent(actor)
        s.verb = xAPI_Statements.__createVerb(uri, body.verb)
        s.object = xAPI_Statements.__createActivity(uri, body.activity, body.activityExtensions)
        s.result = result
        s.context = xAPI_Statements.__createContext(uri, body.contextExtensions)
        s.timestamp = body.timestamp
        return s

    @staticmethod
    def toTinCanExtensions(extensions, uri):
        obj = []
        for ext in extensions.extensions:
            extension = ext[0]
            id = extension.createValidId(uri)
            obj.append((id, str(ext[1])))
        return obj

    @staticmethod
    def toTinCanActivityDefinition(definition):
        ad = tincan.ActivityDefinition()
        ad.name = tincan.LanguageMap(definition.names)
        ad.description = tincan.LanguageMap(definition.descriptions)
        return ad