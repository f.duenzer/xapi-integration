﻿from abc import ABC
from abc import abstractmethod

class xAPI_Extensions(ABC):
    @abstractmethod
    def __init__(self, extensionType: str, context: str):
        self.__context = context
        self.__extensionType = extensionType
        self.__extensions = []

    @property
    def extensions(self):
        return self.__extensions

    @property
    def context(self):
        return self.__context

    @property
    def extensionType(self):
        return self.__extensionType

    def add(self, key, value):
        if key != None:
            self.__extensions.append((key, value))

    def addPair(self, pair):
        if pair != None and pair[0] != None:
            self.__extensions.append(pair)

#    def add(self, *extensions):
#        for pairs in extensions:
#            for pair in pairs:
#                self.add(pair[0], pair[1])

    def remove(self, key):
        extensions = [*filter(lambda ext: ext[0] == key, self.__extensions)]
        self.__extensions.remove(extensions[0])

    def clear(self):
        self.__extensions.clear()