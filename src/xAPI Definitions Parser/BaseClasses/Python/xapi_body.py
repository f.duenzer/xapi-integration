﻿class xAPI_Body:
    def __init__(
        self,
        verb,
        activity,
        activityExtensions = None,
        contextExtensions = None,
        resultExtensions = None,
        completion = None,
        success = None,
        maxScore = None,
        minScore = None,
        rawScore = None,
        scaledScore = None,
        instructor = None,
        timestamp = None):
        self.__maxScore = maxScore
        self.__minScore = minScore
        self.__rawScore = rawScore
        self.__scaledScore = scaledScore
        self.__success = success
        self.__completion = completion
        self.__verb = verb
        self.__activity = activity
        self.__activityExtensions = activityExtensions
        self.__resultExtensions = resultExtensions
        self.__contextExtensions = contextExtensions
        self.__instructor = instructor
        self.__timestamp = timestamp

    @property
    def verb(self):
        return self.__verb

    @property
    def activity(self):
        return self.__activity

    @property
    def activityExtensions(self):
        return self.__activityExtensions

    @property
    def contextExtensions(self):
        return self.__contextExtensions

    @property
    def resultExtensions(self):
        return self.__resultExtensions

    @property
    def completion(self):
        return self.__completion

    @property
    def success(self):
        return self.__success

    @property
    def maxScore(self):
        return self.__maxScore

    @property
    def minScore(self):
        return self.__minScore

    @property
    def rawScore(self):
        return self.__rawScore

    @property
    def scaledScore(self):
        return self.__scaledScore

    @property
    def instructor(self):
        return self.__instructor

    @property
    def timestamp(self):
        return self.__timestamp

    def __str__(self):
        score = f'[max={self.maxScore}, min={self.minScore}, raw={self.rawScore}, scaled={self.scaledScore}]'
        return f'[xAPI_Body: verb={self.verb}, activity={self.activity}, score={score}, success={self.success},'
        + 'completion={self.completion}, instructor={self.instructor}, extensions=[activity={self.activityExtensions},'
        + 'context={self.contextExtensions}, result={self.resultExtensions}], timestamp={self.timestamp}]'