﻿import { xAPI_Definition } from './xapi_definition.js';

export class xAPI_Activity extends xAPI_Definition {
    constructor(context, key, names, descriptions) {
        super(context, key, names, descriptions);
    }

    getPath() {
        return `/${this.context}/activites/${this.key}`;
    }
}