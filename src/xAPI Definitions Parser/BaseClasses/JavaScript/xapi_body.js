﻿export function xAPI_Body(
    verb,
    activity,
    activityExtensions = null,
    contextExtensions = null,
    resultExtensions = null,
    completion = null,
    success = null,
    maxScore = null,
    minScore = null,
    rawScore = null,
    scaledScore = null,
    instructor = null,
    timestamp = null) {
    this.maxScore = maxScore;
    this.minScore = minScore;
    this.rawScore = rawScore;
    this.scaledScore = scaledScore;
    this.success = success;
    this.completion = completion;
    this.verb = verb;
    this.activity = activity;
    this.activityExtensions = activityExtensions;
    this.resultExtensions = resultExtensions;
    this.contextExtensions = contextExtensions;
    this.instructor = instructor;
    this.timestamp = timestamp;
}

xAPI_Body.prototype.toString = function () {
    var score = `[max=${this.maxScore}, min=${this.minScore}, raw=${this.rawScore}, scaled=${this.scaledScore}]`;
    return `[xAPI_Body: verb=${this.verb}, activity=${this.activity}, score=${score}, success=${this.success},`
        + `completion=${this.completion}, instructor=${this.instructor}, extensions=[activity=${this.activityExtensions},`
        + `context=${this.contextExtensions}, result=${this.resultExtensions}], timestamp=${this.timestamp}]`;
}