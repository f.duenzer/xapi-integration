﻿export class xAPI_Extensions {
    #extensions = [];
    #context;
    #extensionType;

    constructor(extensionType, context) {
        if (new.target === xAPI_Extensions) {
            throw new TypeError('Cannot construct xAPI_Extensions instances directly.');
        }
        this.#context = context;
        this.#extensionType = extensionType;
    }

    get extensions() {
        return this.#extensions;
    }

    get context() {
        return this.#context;
    }

    get extensionType() {
        return this.#extensionType;
    }

    add(key, value) {
        if (key != null) {
            this.#extensions.push({ key: key, value: value });
        }
    }

    addPair(pair) {
        if (pair != null && pair.key != null) {
            this.#extensions.push(pair);
        }
    }

    /*add(...extensions) {
        for (const pairs of extensions) {
            for (const pair in pairs) {
                this.add(pair.key, pair.value);
            }
        }
    }*/

    remove(key) {
        var extension = this.#extensions.filter(ext => ext.key == key)[0];
        this.#extensions.splice(this.#extensions.indexOf(extension), 1);
    }

    clear() {
        this.#extensions = [];
    }
}