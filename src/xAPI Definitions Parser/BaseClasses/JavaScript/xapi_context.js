﻿export class xAPI_Context {
    #contextName;

    constructor(name) {
        if (new.target === xAPI_Context) {
            throw new TypeError('Cannot construct xAPI_Context instances directly.');
        }
        this.#contextName = name;
    }

    getActivity(name) {
        return this['activities'][name];
    }

    getActivities(name) {
        return this['activities'][name];
    }

    getVerb(name) {
        return this['verbs'][name];
    }

    getVerbs(name) {
        return this['verbs'][name];
    }
}