﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using xAPI_Definitions_Parser.Code.SyntaxTree;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser.Code.JavaScript {
    /// <summary>
    /// Generates JavaScript code files from the xAPI definitions
    /// </summary>
    public class JavaScriptGenerator : CodeGenerator {
        public override string CoreDirName => base.CoreDirName.ToLower();
        public override string DefinitionsDirName => base.DefinitionsDirName.ToLower();

        public override CodeLanguage Language => CodeLanguage.JavaScript;

        public override string Namespace => default;

        public override string NamespaceCore => default;

        public override string NamespaceDefinitions => default;

        /// <summary>
        /// Creates a JavaScriptGenerator
        /// </summary>
        public JavaScriptGenerator()
            : base(new JavaScriptSerializer()) { }

        protected override string CreateFileName(string name) => base.CreateFileName(name).ToLower();

        protected override List<Import> CreateActivitiesImports(List<string> classes) {
            var imports = new List<Import> {
                new Import(ActivityClassName, $"../{CoreDirName}/{ActivityClassName.ToLower()}.js"),
                new Import(ActivitiesClassName, $"../{CoreDirName}/{ActivitiesClassName.ToLower()}.js")
            };

            foreach (var cls in classes) {
                imports.Add(new Import(cls, $"./{cls.ToLower()}.js"));
            }

            return imports;
        }

        protected override List<Import> CreateVerbsImports(List<string> classes) {
            var imports = new List<Import> {
                new Import(VerbClassName, $"../{CoreDirName}/{VerbClassName.ToLower()}.js"),
                new Import(VerbsClassName, $"../{CoreDirName}/{VerbsClassName.ToLower()}.js")
            };

            foreach (var cls in classes) {
                imports.Add(new Import(cls, $"./{cls.ToLower()}.js"));
            }

            return imports;
        }

        protected override List<Import> CreateExtensionsImports(string type, List<string> classes) {
            var importName = BuildExtensionTypeClassName(type.Capitalize());
            var imports = new List<Import> {
                new Import(
                    importName,
                    $"./{importName.ToLower()}.js"),
                new Import(
                    ExtensionClassName,
                    $"../{CoreDirName}/{ExtensionClassName.ToLower()}.js"
                    )
            };

            foreach (var cls in classes) {
                imports.Add(new Import(cls, $"./{cls.ToLower()}.js"));
            }

            return imports;
        }

        protected override List<Import> CreateContextImports(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var verbsName = BuildVerbsClassName(cxtCap);
            var activitiesName = BuildActivitiesClassName(cxtCap);
            var contextExtensionsName = BuildContextExtensionsClassName(cxtCap);
            return new List<Import> {
                new Import(ContextClassName, $"../{CoreDirName}/{ContextClassName.ToLower()}.js"),
                new Import(verbsName, $"./{verbsName.ToLower()}.js"),
                new Import(activitiesName, $"./{activitiesName.ToLower()}.js"),
                new Import(contextExtensionsName, $"./{contextExtensionsName.ToLower()}.js")
            };
        }

        protected override List<Import> CreateContextExtensionsImports(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var imports = new List<Import>();
            foreach (var extType in context.ExtensionTypes) {
                var extensionsName = BuildExtensionsClassName(extType.Capitalize(), cxtCap);
                imports.Add(new Import(
                    extensionsName,
                    $"./{extensionsName.ToLower()}.js"));
            }
            return imports;
        }

        protected override List<Import> CreateDefinitionsImports(List<xAPIContext> contexts) {
            var imports = new List<Import>();
            foreach (var context in contexts) {
                var cxtCap = context.Name.Capitalize();
                var contextName = BuildContextClassName(cxtCap);
                imports.Add(new Import(
                    contextName,
                    $"./{contextName.ToLower()}.js"));
            }
            return imports;
        }

        protected override List<Import> CreateExtensionTypeImports() {
            return new List<Import> {
                new Import(ExtensionsClassName, $"../{CoreDirName}/{ExtensionsClassName.ToLower()}.js")
            };
        }

        protected override MethodBody CreateExtensionsMethodBody(xAPIExtension extension) {
            var args = string.Format("{0}this.context,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}this.extensionType,{1}", Indent(2), Environment.NewLine)
                + string.Format("{0}\"{1}\",{2}", Indent(2), extension.DefName, Environment.NewLine)
                + string.Format("{0}{1},{2}", Indent(2), Serializer.DictionaryToString(extension.Names, 2), Environment.NewLine)
                + string.Format("{0}{1}", Indent(2), Serializer.DictionaryToString(extension.Descriptions, 2));
            return new MethodBody()
                .AddStatement(new Statement($"this.add(new {ExtensionClassName}({Environment.NewLine}{args}),{Environment.NewLine}{Indent(1)}value);"))
                .AddStatement(new Statement("return this;"));
        }

        protected override MethodBody CreateContextExtensionsGetterBody(xAPIContext context, string type) {
            return new MethodBody()
                .AddStatement(new Statement($"return new {BuildExtensionsClassName(type.Capitalize(), context.Name.Capitalize())}();"));
        }

        public override FileMeta GenerateExtensionTypeFile(string type) {
            var name = BuildExtensionTypeClassName(type.Capitalize());
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var cls = new Class(name, ExtensionsClassName)
                .AddModifier(Modifier.Public)
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddParameter(new Parameter("context", "string", new Null()))
                    .AddBaseArgument(new Argument($"\"{type}\""))
                    .AddBaseArgument(new Argument("context")));
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateExtensionTypeImports());
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        public override CompilationResult Compile(List<FileMeta> coreFiles, List<FileMeta> defFiles, CompilationArguments args) {
            var index = string.Empty;
            //TODO: core and definitions from variable
            foreach (var file in coreFiles) {
                var name = System.IO.Path.GetFileNameWithoutExtension(file.Name);
                index += $"exports.{name} = require(\"./core/{name}.js\");{Environment.NewLine}";
            }
            foreach (var file in defFiles) {
                var name = System.IO.Path.GetFileNameWithoutExtension(file.Name);
                index += $"exports.{name} = require(\"./definitions/{name}.js\");{Environment.NewLine}";
            }
            System.IO.File.WriteAllText(System.IO.Path.Combine(args.Destination, "index.js"), index);
            //TODO: doesnt work?
            Process(args.Destination, "npm.cmd", "init -y");
            Process(args.Destination, "npm.cmd", "install --save @assemblyscript/loader");
            Process(args.Destination, "npm.cmd", "install --save-dev assemblyscript");
            Process(args.Destination, "npx.cmd", "asinit .");
            Process(args.Destination, "npm.cmd", "run asbuild");
            return new JavaScriptCompilationResult(true);
            /*var setup = $"from setuptools import setup{Environment.NewLine}" +
                Environment.NewLine +
                $"setup({Environment.NewLine}" +
                $"{Indent(1)}name='xAPI',{Environment.NewLine}" +
                $"{Indent(1)}version='1.0.0',{Environment.NewLine}" +
                $"{Indent(1)}packages=['core', 'definitions']{Environment.NewLine}" +
                ")";
            System.IO.File.WriteAllText(System.IO.Path.Combine(args.Destination, "setup.py"), setup);
            Process process = new Process();
            process.StartInfo.WorkingDirectory = args.Destination;
            process.StartInfo.FileName = "python";
            process.StartInfo.Arguments = "setup.py sdist bdist_wheel";
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            process.WaitForExit();
            return new PythonCompilationResult(true);*/
            //throw new NotImplementedException();
        }

        private void Process(string workingDir, string fileName, string arguments, bool createNoWindow = true) {
            Process process = new Process();
            process.StartInfo.WorkingDirectory = workingDir;
            process.StartInfo.FileName = fileName;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.CreateNoWindow = createNoWindow;
            process.Start();

            process.WaitForExit();
        }
    }
}