﻿namespace xAPI_Definitions_Parser.Code {
    /// <summary>
    /// Contains the result of the CodeGenerator's compile method
    /// </summary>
    public abstract class CompilationResult {
        /// <summary>
        /// Was the compilation successful or not
        /// </summary>
        public bool Success { get; }

        public CompilationResult(bool success) {
            Success = success;
        }
    }
}