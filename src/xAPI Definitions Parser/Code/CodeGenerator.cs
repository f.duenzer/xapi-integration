﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using xAPI_Definitions_Parser.Code.SyntaxTree;
using xAPI_Definitions_Parser.Definitions;
using xAPI_Definitions_Parser.IO;

namespace xAPI_Definitions_Parser.Code {
    /// <summary>
    /// Generates CodeFiles from the xAPI definitions
    /// </summary>
    public abstract class CodeGenerator {
        public virtual string CoreDirName => "Core";
        public virtual string DefinitionsDirName => "Definitions";

        protected virtual string ActivityClassName => "xAPI_Activity";
        protected virtual string ActivitiesClassName => "xAPI_Activities";
        protected virtual string VerbClassName => "xAPI_Verb";
        protected virtual string VerbsClassName => "xAPI_Verbs";
        protected virtual string ExtensionClassName => "xAPI_Extension";
        protected virtual string ExtensionsClassName => "xAPI_Extensions";
        protected virtual string DefinitionsClassName => "xAPI_Definitions";
        protected virtual string ContextClassName => "xAPI_Context";

        protected virtual string BuildActivitiesClassName(params string[] args) {
            var str = ActivitiesClassName;
            foreach (var arg in args) {
                str += "_" + arg;
            }
            return str;
        }

        protected virtual string BuildVerbsClassName(params string[] args) {
            var str = VerbsClassName;
            foreach (var arg in args) {
                str += "_" + arg;
            }
            return str;
        }

        protected virtual string ExtendClassName(string baseName, params string[] args) {
            foreach (var arg in args) {
                baseName += "_" + arg;
            }
            return baseName;
        }

        protected virtual string BuildExtensionTypeClassName(string type) => ExtensionsClassName + "_" + type;

        protected virtual string BuildExtensionsClassName(string type, params string[] args) {
            var str = BuildExtensionTypeClassName(type);
            foreach (var arg in args) {
                str += "_" + arg;
            }
            return str;
        }

        protected virtual string BuildContextClassName(string context) => ContextClassName + "_" + context;

        protected virtual string BuildContextExtensionsClassName(string context) => BuildContextClassName(context) + "_Extensions";

        /// <summary>
        /// Language of the generator
        /// </summary>
        public abstract CodeLanguage Language { get; }

        /// <summary>
        /// Namespace of the generated code (can be null)
        /// </summary>
        public abstract string Namespace { get; }
        /// <summary>
        /// Core namespace of the generated code (can be null)
        /// </summary>
        public abstract string NamespaceCore { get; }
        /// <summary>
        /// Definitions namespace of the generated code (can be null)
        /// </summary>
        public abstract string NamespaceDefinitions { get; }

        /// <summary>
        /// Serializer for the language
        /// </summary>
        protected SyntaxTreeSerializer Serializer;

        /// <summary>
        /// Logging action
        /// </summary>
        public event Action<string> OnLog;

        protected void Log(string message) => OnLog?.Invoke(message);

        /// <summary>
        /// Creates a CodeGenerator
        /// </summary>
        /// <param name="serializer">Serializer to use in the generator</param>
        public CodeGenerator(SyntaxTreeSerializer serializer) {
            Serializer = serializer;
        }

        /// <summary>
        /// Create indentation whitespace of specified depth
        /// </summary>
        /// <param name="depth">Intendation depth</param>
        /// <returns>String with whitespace</returns>
        public static string Indent(int depth) {
            return new String(' ', 4 * depth);
        }

        /// <summary>
        /// Compiles the generated code files
        /// </summary>
        /// <param name="coreFiles">xAPI JSON core files to generate from</param>
        /// <param name="defFiles">xAPI JSON definitions files to generate from</param>
        /// <param name="args">Arguments for the compilation process</param>
        /// <returns>Results of the compilation process</returns>
        public abstract CompilationResult Compile(List<FileMeta> coreFiles, List<FileMeta> defFiles, CompilationArguments args);

        /// <summary>
        /// Generates files from contexts
        /// </summary>
        /// <param name="contexts">List of contexts to generate from</param>
        /// <returns>List of generated files</returns>
        public virtual List<FileMeta> GenerateContexts(List<xAPIContext> contexts) {
            var files = new List<FileMeta>();
            var extTypes = new string[] { "activity", "context", "result" };
            if (contexts.Count > 0) {
                foreach (var context in contexts) {
                    var contextFiles = GenerateContext(context);
                    foreach (var contextFile in contextFiles) {
                        files.Add(contextFile);
                    }
                    /*foreach (var extType in context.ExtensionTypes) {
                        if (!extTypes.Contains(extType)) {
                            extTypes.Add(extType);
                        }
                    }*/
                }
                files.Add(GenerateDefinitionsFile(contexts));
                foreach (var extType in extTypes) {
                    files.Add(GenerateExtensionTypeFile(extType));
                }
            }
            return files;
        }

        /// <summary>
        /// Generates core files
        /// </summary>
        /// <returns>List of generated files</returns>
        public virtual List<FileMeta> GenerateCoreFiles() {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            var resources = assembly.GetManifestResourceNames()
                .Where(res => Path.GetExtension(res).Equals(Language.GetExtension())).ToList();

            var files = new List<FileMeta>();

            if (resources != null) {
                foreach (var resource in resources) {
                    string content = null;
                    using (var stream = assembly.GetManifestResourceStream(resource)) {
                        using (var reader = new StreamReader(stream)) {
                            content = reader.ReadToEnd();
                        }
                    }

                    if (content != null) {
                        files.Add(new FileMeta(resource.Replace($"xAPI_Definitions_Parser.BaseClasses.{Language.GetName()}.", ""), null, content));
                    }
                }
            }
            return files;
        }

        /// <summary>
        /// Generates files from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>List of generated files</returns>
        public virtual List<FileMeta> GenerateContext(xAPIContext context) {
            Log($"{GetType().Name} generating context: {context.Name}");
            var files = new List<FileMeta>();
            files.AddRange(GenerateActivitiesFiles(context));
            files.AddRange(GenerateVerbsFiles(context));
            files.AddRange(GenerateExtensions(context));
            files.Add(GenerateContextFile(context));
            files.Add(GenerateContextExtensionsFile(context));
            return files;
        }

        /// <summary>
        /// Generates Extensions files from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>List of generated files</returns>
        public virtual List<FileMeta> GenerateExtensions(xAPIContext context) {
            var codeFiles = new List<FileMeta>();
            foreach (var extType in context.ExtensionTypes) {
                codeFiles.AddRange(GenerateExtensionsFiles(context, extType));
            }
            return codeFiles;
        }

        /// <summary>
        /// Creates a class for definitions files
        /// </summary>
        /// <typeparam name="T">Type of definition</typeparam>
        /// <param name="context">Context to create a definitions file of</param>
        /// <param name="name">Name of the class</param>
        /// <param name="definitions">Definitions to create an object of</param>
        /// <param name="defType">Definition type</param>
        /// <param name="defTypePlural">Plural of the definition type</param>
        /// <returns>Created class</returns>
        protected virtual Class CreateDefinitionsClass<T>(string context, string name, xAPIDefinitionsList<T> definitions,
            string defType, string defTypePlural) where T : xAPIDefinition {
            var properties = new List<Property>();

            foreach (var subDefinitions in definitions.SubDefinitions) {
                properties.Add(new Property(subDefinitions.Key, ExtendClassName(name, subDefinitions.Key.Capitalize()))
                    .AddModifier(Modifier.Public));
            }
            foreach (var definition in definitions.Definitions) {
                properties.Add(new Property(definition.DefName, defType)
                    .AddModifier(Modifier.Public)
                    .AddArgument(new Argument($"\"{context}\"", "context"))
                    .AddArgument(new Argument($"\"{definition.DefName}\"", "key"))
                    .AddArgument(new Argument(definition.Names, "names"))
                    .AddArgument(new Argument(definition.Descriptions, "descriptions"))
                    .WithComment(definition.GetDescription("en-US", true)));
            }

            return new Class(name, defTypePlural, $"Provides the {defTypePlural} of the context {context} as public properties.")
                .AddModifier(Modifier.Public)
                .AddProperties(properties)
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddBaseArgument(new Argument($"\"{context}\"")));
        }

        /// <summary>
        /// Creates the full file name (override in subclasses to enforce naming conventions)
        /// </summary>
        /// <param name="name">Name of the file without extensions</param>
        /// <returns>File name with extension</returns>
        protected virtual string CreateFileName(string name) => name + Language.GetExtension();

        /// <summary>
        /// Generates Activities files from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>Generated files</returns>
        public virtual List<FileMeta> GenerateActivitiesFiles(xAPIContext context) {
            return GenerateActivitiesFiles(context.Name, context.Activities);
        }

        /// <summary>
        /// Generates Activities files from a context and a list of activites
        /// </summary>
        /// <param name="context">Context to create activities files of</param>
        /// <param name="activities">Activities to create files of</param>
        /// <param name="name">Name of the class</param>
        /// <returns>List of generated files</returns>
        protected virtual List<FileMeta> GenerateActivitiesFiles(string context, xAPIDefinitionsList<xAPIActivity> activities, string name = default) {
            var codeFiles = new List<FileMeta>();

            if (string.IsNullOrEmpty(name)) {
                name = BuildActivitiesClassName(context.Capitalize());
            }
            
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var classesImport = new List<string>();

            foreach (var subDefinitions in activities.SubDefinitions) {
                var subName = ExtendClassName(name, subDefinitions.Key.Capitalize());
                codeFiles.AddRange(GenerateActivitiesFiles(context, subDefinitions.Value, subName));
                classesImport.Add(subName);
            }

            var cls = CreateDefinitionsClass(context, name,
                    activities, ActivityClassName, ActivitiesClassName);
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateActivitiesImports(classesImport));

            codeFiles.Add(new FileMeta(
                    fileName,
                    null,
                    Serializer.RootToString(root)));

            return codeFiles;
        }

        /// <summary>
        /// Generates Verbs files from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>Generated files</returns>
        public virtual List<FileMeta> GenerateVerbsFiles(xAPIContext context) {
            return GenerateVerbsFiles(context.Name, context.Verbs);
        }

        protected virtual List<FileMeta> GenerateVerbsFiles(string context, xAPIDefinitionsList<xAPIVerb> verbs, string name = default) {
            var codeFiles = new List<FileMeta>();

            if (string.IsNullOrEmpty(name)) {
                name = BuildVerbsClassName(context.Capitalize());
            }
            
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var classesImport = new List<string>();

            foreach (var subDefinitions in verbs.SubDefinitions) {
                var subName = ExtendClassName(name, subDefinitions.Key.Capitalize());
                codeFiles.AddRange(GenerateVerbsFiles(context, subDefinitions.Value, subName));
                classesImport.Add(subName);
            }

            var cls = CreateDefinitionsClass(context, name,
                    verbs, VerbClassName, VerbsClassName);
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateVerbsImports(classesImport));

            codeFiles.Add(new FileMeta(
                    fileName,
                    null,
                    Serializer.RootToString(root)));

            return codeFiles;
        }

        /// <summary>
        /// Generates Extensions files from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <param name="type">Type to generate for</param>
        /// <returns>Generated files</returns>
        public virtual List<FileMeta> GenerateExtensionsFiles(xAPIContext context, string type) {
            return GenerateExtensionsFiles(context.Name, type, context.Extensions[type]);
        }

        private List<FileMeta> GenerateExtensionsFiles(string context, string type, xAPIDefinitionsList<xAPIExtension> extensions, string name = default) {
            var codeFiles = new List<FileMeta>();

            var typCap = type.Capitalize();
            var baseName = BuildExtensionTypeClassName(typCap);

            if (string.IsNullOrEmpty(name)) {
                name = BuildExtensionsClassName(typCap, context.Capitalize());
            }

            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var classesImport = new List<string>();

            foreach (var subExtensions in extensions.SubDefinitions) {
                var subName = ExtendClassName(name, subExtensions.Key.Capitalize());
                codeFiles.AddRange(GenerateExtensionsFiles(context, type, subExtensions.Value, subName));
                classesImport.Add(subName);
            }

            var cls = new Class(name, baseName, $"Provides all extensions of the context {context} of type {type} as public properties.")
                .AddModifier(Modifier.Public)
                .AddProperties(CreateExtensionsProperties(name, extensions))
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddBaseArgument(new Argument($"\"{context}\""))
                    .WithBody(new MethodBody()))
                .AddMethods(CreateExtensionsMethods(extensions, name));
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateExtensionsImports(type, classesImport));

            codeFiles.Add(new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root)));

            return codeFiles;
        }

        /// <summary>
        /// Creates properties for an extensions file
        /// </summary>
        /// <param name="name">Name of the class</param>
        /// <param name="extensions">List of extensions to create properties of</param>
        /// <returns>List of created properties</returns>
        protected virtual List<Property> CreateExtensionsProperties(string name, xAPIDefinitionsList<xAPIExtension> extensions) {
            var properties = new List<Property>();

            foreach (var subExtensions in extensions.SubDefinitions) {
                properties.Add(new Property(subExtensions.Key, ExtendClassName(name, subExtensions.Key.Capitalize()))
                    .AddModifier(Modifier.Public));
            }

            return properties;
        }

        /// <summary>
        /// Creates Methods for an extensions file
        /// </summary>
        /// <param name="extensions">List of extensions to create methods of</param>
        /// <param name="name">Name of the class</param>
        /// <returns>List of created methods</returns>
        protected virtual List<Method> CreateExtensionsMethods(xAPIDefinitionsList<xAPIExtension> extensions, string name) {
            var methods = new List<Method>();
            foreach (var extension in extensions.Definitions) {
                methods.Add(new Method(extension.DefName, name)
                    .AddModifier(Modifier.Public)
                    .AddModifier(Modifier.Virtual)
                    .AddParameter(new Parameter("value", "object"))
                    .WithBody(CreateExtensionsMethodBody(extension)));
            }
            return methods;
        }

        /// <summary>
        /// Generates Context file from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>Generated file</returns>
        public virtual FileMeta GenerateContextFile(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var name = BuildContextClassName(cxtCap);
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var cls = new Class(name, ContextClassName, $"Provides the definitions of the context {context.Name} as public properties.")
                .AddModifier(Modifier.Public)
                .AddProperty(new Property("verbs", BuildVerbsClassName(cxtCap))
                    .AddModifier(Modifier.Public))
                .AddProperty(new Property("activities", BuildActivitiesClassName(cxtCap))
                    .AddModifier(Modifier.Public))
                .AddProperty(new Property("extensions", BuildContextExtensionsClassName(cxtCap))
                    .AddModifier(Modifier.Public))
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddBaseArgument(new Argument($"\"{context.Name}\"")));
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateContextImports(context));
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        /// <summary>
        /// Generates ContextExtensions file from a context
        /// </summary>
        /// <param name="context">Context to generate from</param>
        /// <returns>Generated file</returns>
        public virtual FileMeta GenerateContextExtensionsFile(xAPIContext context) {
            var cxtCap = context.Name.Capitalize();
            var name = BuildContextExtensionsClassName(cxtCap);
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var getableProperties = new List<GetableProperty>();

            foreach (var extType in context.ExtensionTypes) {
                getableProperties.Add(new GetableProperty(extType, BuildExtensionsClassName(extType.Capitalize(), cxtCap))
                    .AddModifier(Modifier.Public)
                    .AddModifier(Modifier.Virtual)
                    .WithGetter(new Getter(CreateContextExtensionsGetterBody(context, extType))));
            }

            var cls = new Class(name, null, $"Provides the extensions of the context {context.Name} as public properties.")
                .AddModifier(Modifier.Public)
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public))
                .AddGetableProperties(getableProperties);
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateContextExtensionsImports(context));
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        /// <summary>
        /// Generates Definitions file from a context
        /// </summary>
        /// <param name="contexts">Contexts to generate from</param>
        /// <returns>Generated file</returns>
        public virtual FileMeta GenerateDefinitionsFile(List<xAPIContext> contexts) {
            var fileName = CreateFileName(DefinitionsClassName);
            Log($"{GetType().Name} generating: {fileName}");
            var properties = new List<Property>();
            foreach (var context in contexts) {
                properties.Add(new Property(context.Name,BuildContextClassName(context.Name.Capitalize()))
                    .AddModifier(Modifier.Public)
                    .AddModifier(Modifier.Static));
            }

            var cls = new Class(DefinitionsClassName, null, $"Static class that provides all contexts as public properties.")
                .AddModifier(Modifier.Public)
                .AddModifier(Modifier.Static)
                .AddProperties(properties);
            var root = new Root(cls, Namespace)
                .AddImports(CreateDefinitionsImports(contexts));
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        /// <summary>
        /// Generates ExtensionType file from a context
        /// </summary>
        /// <param name="type">Type to generate from</param>
        /// <returns>Generated file</returns>
        public virtual FileMeta GenerateExtensionTypeFile(string type) {
            var name = BuildExtensionTypeClassName(type.Capitalize());
            var fileName = CreateFileName(name);
            Log($"{GetType().Name} generating: {fileName}");

            var cls = new Class(name, ExtensionsClassName, $"Provides all extensions of type {type} as public properties.")
                .AddModifier(Modifier.Public)
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddParameter(new Parameter("context", "string"))
                    .AddBaseArgument(new Argument($"\"{type}\""))
                    .AddBaseArgument(new Argument("context")))
                .AddConstructor(new Constructor(name)
                    .AddModifier(Modifier.Public)
                    .AddBaseArgument(new Argument($"\"{type}\""))
                    .AddBaseArgument(new Argument(new Null())));
            var root = new Root(cls, NamespaceDefinitions)
                .AddImports(CreateExtensionTypeImports());
            return new FileMeta(
                fileName,
                null,
                Serializer.RootToString(root));
        }

        /// <summary>
        /// Creates the imports for Activities files
        /// </summary>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateActivitiesImports(List<string> classes);
        /// <summary>
        /// Creates the imports for Verbs files
        /// </summary>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateVerbsImports(List<string> classes);
        /// <summary>
        /// Creates the imports for Extensions files
        /// </summary>
        /// <param name="type">Type of the extensions</param>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateExtensionsImports(string type, List<string> classes);
        /// <summary>
        /// Creates the imports for Context files
        /// </summary>
        /// <param name="context">Context of the file</param>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateContextImports(xAPIContext context);
        /// <summary>
        /// Creates the imports for ContextExtensions files
        /// </summary>
        /// <param name="context">Context of the file</param>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateContextExtensionsImports(xAPIContext context);
        /// <summary>
        /// Creates the imports for Definitions files
        /// </summary>
        /// <param name="contexts">Contexts of the definitions</param>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateDefinitionsImports(List<xAPIContext> contexts);
        /// <summary>
        /// Creates the imports for ExtensionType files
        /// </summary>
        /// <returns>Created imports</returns>
        protected abstract List<Import> CreateExtensionTypeImports();

        /// <summary>
        /// Creates a MethodBody for Extensions files
        /// </summary>
        /// <param name="extension">Extension to create the body for</param>
        /// <returns>Created MethodBody</returns>
        protected abstract MethodBody CreateExtensionsMethodBody(xAPIExtension extension);
        /// <summary>
        /// Creates a MethodBody for the getter of a ContextExtensions file
        /// </summary>
        /// <param name="context">Context to create the file for</param>
        /// <param name="type">ExtensionType of the Getter</param>
        /// <returns>Created MethodBody</returns>
        protected abstract MethodBody CreateContextExtensionsGetterBody(xAPIContext context, string type);
    }
}