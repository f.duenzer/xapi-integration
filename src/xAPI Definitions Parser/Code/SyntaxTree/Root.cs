﻿using System.Collections.Generic;

namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// Root of the syntax tree
    /// </summary>
    public class Root {
        private Class cls;
        private string ns;
        private List<Import> imports = new List<Import>();

        /// <summary>
        /// Class in the syntax tree
        /// </summary>
        public ref readonly Class Class => ref cls;
        /// <summary>
        /// Namespace in the syntax tree
        /// </summary>
        public string Namespace => ns;
        /// <summary>
        /// Imports in the syntax tree
        /// </summary>
        public IReadOnlyList<Import> Imports => imports.AsReadOnly();

        public Root() { }

        public Root(Class cls, string ns = default) {
            this.cls = cls;
            this.ns = ns;
        }

        public Root WithClass(Class cls) {
            this.cls = cls;
            return this;
        }

        public Root WithNamespace(string ns) {
            this.ns = ns;
            return this;
        }

        public Root AddImport(Import import) {
            imports.Add(import);
            return this;
        }

        public Root AddImports(params Import[] imports) {
            this.imports.AddRange(imports);
            return this;
        }

        public Root AddImports(IEnumerable<Import> imports) {
            this.imports.AddRange(imports);
            return this;
        }
    }
}