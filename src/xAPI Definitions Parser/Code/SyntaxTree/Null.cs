﻿namespace xAPI_Definitions_Parser.Code.SyntaxTree {
    /// <summary>
    /// A null object in the syntax tree
    /// </summary>
    public class Null {
    }
}